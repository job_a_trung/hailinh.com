<?php
include 'header.php';
?>
<div class="clear"></div>
<div class="main_wrapper">
    <div class="breadcrumbs">
        <div class="container">
            <div class="breadcrumbs_wrapper" itemscope="" itemtype="http://schema.org/WebPage">
                <ul class="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                        <a title="Hải Linh" href="https://hailinh.vn/" itemprop="item">
                            <span itemprop="name">Trang chủ</span>
                            <meta content="1" itemprop="position">
                        </a>
                    </li>
                    <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                        <a title="Gạch ốp lát" href="https://hailinh.vn/gach-op-lat-pc83.html" itemprop="item">
                            <span itemprop="name">Gạch ốp lát</span>
                            <meta content="2" itemprop="position">
                        </a>
                        <div class="parent_sub cls">
                            <a href="https://hailinh.vn/gach-lat-nen-pc151.html" title="Gạch lát nền">Gạch lát nền</a>
                            <a href="https://hailinh.vn/gach-op-tuong-pc150.html" title="Gạch ốp tường">Gạch ốp tường</a>
                            <a href="https://hailinh.vn/gach-mosaic-pc295.html" title="Gạch Mosaic">Gạch Mosaic</a>
                            <a href="https://hailinh.vn/gach-cnc-pc344.html" title="Gạch CNC">Gạch CNC</a>
                        </div>
                    </li>
                    <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                        <a title="Gạch lát nền" href="https://hailinh.vn/gach-lat-nen-pc151.html" itemprop="item">
                            <span itemprop="name">Gạch lát nền</span>
                            <meta content="3" itemprop="position">
                        </a>
                        <div class="parent_sub cls">
                            <a href="https://hailinh.vn/gach-lat-nen-15x60-pc243.html" title="Gạch lát nền 15x60">Gạch lát nền 15x60</a>
                            <a href="https://hailinh.vn/gach-lat-nen-15x90-pc175.html" title="Gạch lát nền 15x90">Gạch lát nền 15x90</a>
                            <a href="https://hailinh.vn/gach-lat-nen-20x60-pc222.html" title="Gạch lát nền 20x60">Gạch lát nền 20x60</a>
                        </div>
                    </li>
                    <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                        <a title="Gạch lát nền 60x60" href="https://hailinh.vn/gach-lat-nen-60x60-pc156.html" itemprop="item">
                            <span itemprop="name">Gạch lát nền 60x60</span>
                            <meta content="4" itemprop="position">
                        </a>
                    </li>
                    <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                        <a title="Taicera" href="https://hailinh.vn/gach-lat-nen-taicera-60x60-pcm156.html" itemprop="item">
                            <span itemprop="name">Taicera</span>
                            <meta content="5" itemprop="position">
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="container container_main_wrapper">
        <div class="main-area main-area-1col main-area-full">
            <div class="product" itemscope="" itemtype="https://schema.org/Product">
                <meta itemprop="url" content="https://hailinh.vn/gach-lat-nen-60x60/gach-taicera-g68985-p4617.html">
                <div class="detail_main cls">
                    <div class="detail_main_top cls">
                        <div class="frame_left frame_left_animate">
                            <div class="frame_img">
                                <div class="frame_img_inner">
                                    <div class="magic_zoom_area">
                                        <a id="Zoomer" href="javascript:void(0)" data-image="https://hailinh.vn/images/products/2020/08/14/large/g68985_1597395124.jpg" class="MagicZoomPlus" title="">
                                            <img onclick="gotoGallery(1,0,0);" src="https://hailinh.vn/images/products/2020/08/14/large/g68985_1597395124.jpg" title="" style="">
                                        </a>
                                    </div>
                                    <div id="sync1_wrapper">
                                        <div id="sync1">
                                            <div class="item">
                                                <a href="https://hailinh.vn/images/products/2020/10/12/original/bl1140_2_1602492274.jpg" class="color_thump_item ">
                                                    <img src="https://hailinh.vn/images/products/2020/10/12/small/bl1140_2_1602492274.jpg" longdesc="https://hailinh.vn/images/products/2020/10/12/original/bl1140_2_1602492274.jpg" alt="Vòi chậu Belli BL1140" class="image3" itemprop="image" title="" style="">
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a href="javascript:void(0)" id="images/products/2020/09/29/original/bl1140_1601349588.jpg" rel="image_large" class="selected" title="Vòi chậu Belli BL1140">
                                                    <img src="https://hailinh.vn/images/products/2020/09/29/small/bl1140_1601349588.jpg" longdesc="https://hailinh.vn/images/products/2020/09/29/original/bl1140_1601349588.jpg" alt="Vòi chậu Belli BL1140" itemprop="image">
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a href="javascript:void(0)" id="images/products/2020/08/14/original/g68985_1597395124.jpg" rel="image_large" class="selected" title="Gạch Taicera G68985">
                                                    <img src="https://hailinh.vn/images/products/2020/08/14/small/g68985_1597395124.jpg" longdesc="https://hailinh.vn/images/products/2020/08/14/original/g68985_1597395124.jpg" alt="Gạch Taicera G68985" itemprop="image">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbs">
                                    <div id="sync2" class="owl-carousel owl-theme">
                                        <div class="item">
                                            <a href="https://hailinh.vn/images/products/2020/10/12/original/bl1140_2_1602492274.jpg" class="color_thump_item ">
                                                <img src="https://hailinh.vn/images/products/2020/10/12/small/bl1140_2_1602492274.jpg" longdesc="https://hailinh.vn/images/products/2020/10/12/original/bl1140_2_1602492274.jpg" alt="Vòi chậu Belli BL1140" class="image3" itemprop="image" title="" style="">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="javascript:void(0)" id="images/products/2020/09/29/original/bl1140_1601349588.jpg" rel="image_large" class="selected" title="Vòi chậu Belli BL1140">
                                                <img src="https://hailinh.vn/images/products/2020/09/29/small/bl1140_1601349588.jpg" longdesc="https://hailinh.vn/images/products/2020/09/29/original/bl1140_1601349588.jpg" alt="Vòi chậu Belli BL1140" itemprop="image">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="javascript:void(0)" id="images/products/2020/08/14/original/g68985_1597395124.jpg" rel="image_large" class="selected" title="Gạch Taicera G68985">
                                                <img src="https://hailinh.vn/images/products/2020/08/14/small/g68985_1597395124.jpg" longdesc="https://hailinh.vn/images/products/2020/08/14/original/g68985_1597395124.jpg" alt="Gạch Taicera G68985" itemprop="image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide_FT"></div>
                            </div>
                        </div>
                        <div class="frame_center">
                            <div class="product_base">
                                <div class="product_name">
                                    <h1 itemprop="name">Vòi chậu Belli BL1140 </h1>
                                    <span class="rate rate_head" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                        <span class="star-on star">
				                            <svg aria-hidden="true" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18">
                                                <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7
                                                     36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7
                                                     68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382
                                                     150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class="">
                                                </path>
                                            </svg>
			                            </span>
                                        <span class="star-on star">
				                            <svg aria-hidden="true" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18">
                                                <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7
                                                     36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7
                                                     68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382
                                                     150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class="">
                                                </path>
                                            </svg>
			                            </span>
                                        <span class="star-on star">
				                            <svg aria-hidden="true" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18">
                                                <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7
                                                     36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7
                                                     68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382
                                                     150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class="">
                                                </path>
                                            </svg>
			                            </span>
                                        <span class="star-on star">
				                            <svg aria-hidden="true" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18">
                                                <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7
                                                     36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7
                                                     68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382
                                                     150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class="">
                                                </path>
                                            </svg>
			                            </span>
                                        <span class="star-on star">
				                            <svg aria-hidden="true" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18">
                                                <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7
                                                     36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7
                                                     68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382
                                                     150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" class="">
                                                </path>
                                            </svg>
			                            </span>
                                        <span itemprop="ratingValue" class="hide">4.7</span>
                                        <span itemprop="bestRating" class="hide">5</span>
                                        <a href="javascript:void(0)" title="Đánh giá sản phẩm này" class="rate_count" onclick="$('html, body').animate({ scrollTop: $('#prodetails_tab3').offset().top }, 500);">  (
                                            <span itemprop="ratingCount">1</span> đánh giá)
                                        </a>
                                    </span>
                                    <div class="cls manu-sku">
                                        <div class="manu_name">Thương hiệu: Belli</div>
                                        <div class="sku">Mã sản phẩm: BL1140</div>
                                        <div class="status">Tình trạng: <span>Còn hàng</span></div>
                                    </div>
                                </div>
                                <meta itemprop="brand" content="Belli">
                                <!--Bắt đầu mua nhanh -->
                                <form action="#" name="buy_simple_form" method="post">
                                    <div class="clear"></div>
                                    <div class="price cls " itemprop="offers" itemscope="" itemtype="https://schema.org/AggregateOffer">
                                        <link itemprop="availability" href="https://schema.org/InStock">
                                        <div class="price_current " id="price" content="1720000"> 1.720.000₫ </div>
                                        <meta itemprop="lowPrice" content="1720000">
                                        <meta itemprop="highPrice" content="1720000">
                                        <meta itemprop="itemOffered" name="itemOffered" content="10">
                                        <meta itemprop="offerCount" name="offerCount" content="1">
                                        <meta itemprop="priceCurrency" content="VND">
                                        <div class="price_old">
                                            <span class="price_old_nb" id="price_old"
                                                  content="2450000"> 2.450.000₫</span>
                                            <span class="discount">(<span id="discount">-30</span>%)</span>

                                        </div>
                                    </div>
                                    <div class="gift_summary">  </div>
                                    <div>
                                        <meta itemprop="mpn" content="BL1140">
                                        <meta itemprop="sku" content="BL1140">
                                    </div>
                                    <div class="_color">
                                        <input type="hidden" value="" id="color_curent_id" name="color_curent_id">
                                    </div>
                                    <div class="_attributes"> </div>
                                    <div class="detail_button product_detail_bt cls">
                                        <div class="text">Số lượng:</div>
                                        <div class="numbers-row">
                                            <input name="buy_count" id="buy_count" value="1" type="text" placeholder="Số lượng">
                                            <span class="inc button" data="inc">
                                                <svg width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                    <path fill="gray" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947
                                                    256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97
                                                    0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="">
                                                    </path>
                                                </svg>
                                            </span>
                                            <span class="dec button" data="dec">
                                                <svg width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                    <path fill="gray" d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256
                                                    10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97
                                                    0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="">
                                                    </path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="clear"></div>
                                        <button type="submit" class="btn-buy-222 fl" id="buy-now-222">
                                            <span> Mua nhanh</span>
                                        </button>
                                        <a href="javascript:void(0)" onclick="add_to_cart(7874)" class="btn-dathang"
                                           data-toggle="modal">
                                            <font>Thêm vào giỏ hàng</font>
                                        </a>
                                        <div class="clear"></div>
                                    </div>
                                    <input type="hidden" name="module" value="products">
                                    <input type="hidden" name="view" value="cart">
                                    <input type="hidden" name="task" value="ajax_buy_product">
                                    <input type="hidden" name="product_id" value="7874">
                                    <input type="hidden" name="Itemid" value="10">
                                </form>
                                <!-- Kết thúc mua nhanh -->
                                <div class="buy_fast">
                                    <form action="" name="buy_fast_form" id="buy_fast_form" method="post"
                                          onsubmit="javascript: return submit_form_buy_fast();">
                                        <div class="cls">
                                            <input type="tel" required="" value=""
                                                   placeholder="Nhập số điện thoại tư vấn nhanh" id="telephone_buy_fast"
                                                   name="telephone_buy_fast" class="keyword input-text">
                                            <button type="submit" class="button-buy-fast button">Gửi</button>
                                        </div>
                                        <input type="hidden" name="module" value="products">
                                        <input type="hidden" name="view" value="cart">
                                        <input type="hidden" name="task" value="buy_fast_save">
                                        <input type="hidden" name="id" value="7874">
                                        <input type="hidden" name="Itemid" value="10">
                                        <input type="hidden"
                                               value="L3ZvaS1jaGF1LW5vbmctbGFuaC92b2ktY2hhdS1iZWxsaS1ibDExNDAtcDc4NzQuaHRtbA=="
                                               name="return">
                                    </form>
                                </div>
                                <!--	TAGS		-->
                                <input type="hidden" name="record_alias" id="record_alias" value="voi-chau-belli-bl1140">
                                <input type="hidden" name="record_id" id="record_id" value="7874">
                                <input type="hidden" name="table_name" id="table_name" value="voi_chau">
                            </div>
                            <div class="block-strengths-row2-2">
                                <a class="item" title="Bảo hành theo hãng" href="">
                                    <img src="https://hailinh.vn/images/strengths/original/bao-hanh-theo-hang_1607501854.png" alt="Bảo hành theo hãng"> Bảo hành theo hãng
                                </a>
                                <a class="item" title="Hàng chính hãng" href="">
                                    <img src="https://hailinh.vn/images/strengths/original/hang-chinh-hang_1607502148.png" alt="Hàng chính hãng">
                                    Hàng chính hãng
                                </a>
                                <a class="item" title="Vận chuyển toàn quốc" href="">
                                    <img src="https://hailinh.vn/images/strengths/original/ho-tro-van-chuyen-toan-quoc_1607505460.png" alt="Vận chuyển toàn quốc">
                                    Vận chuyển toàn quốc
                                </a>
                                <a class="item" title="Liên hệ để có giá tốt nhất" href="tel:tel:0968121717">
                                    <img src="https://hailinh.vn/images/strengths/original/lien-he-de-co-gia-tot-nhat_1607506260.png" alt="Liên hệ để có giá tốt nhất">
                                    Liên hệ để có giá tốt nhất
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="detail_main_bot cls">
                        <div class="frame_b_l ">
                            <div class="tab_content_right">
                                <div class="characteristic">
                                    <div class="title-box-product">Thông số kỹ thuật</div>
                                    <table class="charactestic_table" border="0" bordercolor="#EEE" cellpadding="7"
                                           width="100%">

                                        <tbody>
                                        <tr class="tr-0">
                                            <td class="title_charactestic" width="40%">
                                                Loại vòi chậu
                                            </td>
                                            <td class="content_charactestic">
                                                Vòi 1 lỗ,
                                            </td>
                                        </tr>
                                        <tr class="tr-1">
                                            <td class="title_charactestic" width="40%">
                                                Xuất xứ
                                            </td>
                                            <td class="content_charactestic">
                                                Việt Nam
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="title-box-product">Đặc điểm của Vòi chậu Belli BL1140</div>
                            <div class="product_tab_content" id="tabs">
                                <div id="prodetails_tab1" class="prodetails_tab prodetails_tab_content">
                                    <div class="">
                                        <div class="description boxdesc" id="boxdesc">
                                            <div id="box_conten_linfo">
                                                <div class="box_conten_linfo_inner" itemprop="description">
                                                    <p style="text-align:justify">
                                                        <span style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px">Vòi chậu nóng lạnh 1 lỗ Belli BL1140 cực kỳ sang trọng, tiện nghi và an toàn sẽ khiến bất cứ ai dùng đều cảm thấy vô cùng hài lòng.&nbsp;</span></span>
                                                    </p>

                                                    <p style="text-align:center"><span
                                                                style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px"><iframe allow=";"
                                                                                                   allowfullscreen=""
                                                                                                   frameborder="0"
                                                                                                   height="360"
                                                                                                   src="https://www.youtube.com/embed/mKI2UDzQN2g"
                                                                                                   width="640"
                                                                                                   data-loader="frame"></iframe></span></span>
                                                    </p>

                                                    <p style="text-align:justify"><span
                                                                style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px">Chiếc vòi chậu nóng lạnh&nbsp;BL1140 thuộc dòng sản phẩm Belli được công ty Hải Linh đặt hàng sản xuất và kiểm soát chặt chẽ trên dây chuyền công nghệ hiện đại của Đức. Chất lượng sản phẩm tuân theo tiêu chuẩn khắt khe ISO 9001:2000.</span></span>
                                                    </p>

                                                    <p style="text-align:justify"><span
                                                                style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px"><strong>Thông số kỹ thuật của vòi chậu nóng lạnh Belli BL1140</strong></span></span>
                                                    </p>

                                                    <table border="1" cellpadding="1" cellspacing="1" style="width:100%"
                                                           class="table_special">
                                                        <tbody>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Tên sản phẩm</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">Vòi chậu Belli BL1140</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Mã sản phẩm</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">BL1140</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Dòng sản phẩm</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">Vòi chậu nóng lạnh 1 lỗ</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Chất liệu</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">Đồng mạ Niken/Chrome. Tay đồng, xi phông nhựa, siết chân nhựa ABS</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Độ dày lớp Chrome</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">0,3 µm</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Áp lực nước</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">0,75 Mpa</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Nhiệt độ nước tối đa</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">90 độ C</span></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px"><strong>Bảo hành</strong></span></span>
                                                            </td>
                                                            <td style="text-align:justify"><span
                                                                        style="font-family:Arial,Helvetica,sans-serif"><span
                                                                            style="font-size:14px">Bảo hành vòi chậu 36 tháng, phụ kiện đi kèm 12 tháng</span></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                    <h2 style="text-align:justify"><span
                                                                style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px"><strong>Đặc điểm nổi bật của vòi chậu 1 lỗ Belli BL1140</strong></span></span>
                                                    </h2>

                                                    <ul>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Vòi chậu làm từ chất liệu đồng nguyên chất cao nên bền vững và cực kỳ an toàn với sức khỏe người dùng.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Tay gạt vòi chậu cũng làm bằng đồng khác hoàn toàn với tay gạt bằng kẽm ở sản phẩm vòi chậu thông thường. Bởi vậy, vòi chậu Belli luôn bền bỉ và dễ dàng sử dụng hơn rất nhiều.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Lõi chia nước được nhập khẩu Hàn Quốc chịu nhiệt cao.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Gioăng cao su đàn hồi tốt, chống mài mòn tăng tuổi thọ sử dụng cho sản phẩm.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Lớp mạ bề mặt vòi rửa được tạo thành bởi chất liệu Chrome/Niken dày 0.3 µm có tính năng chống oxy hóa, ngăn han gỉ, chống bám bẩn.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Đầu vòi chậu thiết kế dạng tổ ong cho dòng nước chảy êm ái, chống bắn và tiết kiệm nước.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Vòi dài với đầu vòi được tạo độ chếch nên đường nước chảy rộng hơn tạo nên sự thoải mái khi sử dụng.</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Kiểu dáng thiết kế vòi phong cách hiện đại với các chi tiết rành mạch, rõ ràng và cực kỳ khỏe khoắn.&nbsp;</span></span>
                                                        </li>
                                                        <li style="text-align:justify"><span
                                                                    style="font-family:Arial,Helvetica,sans-serif"><span
                                                                        style="font-size:14px">Thời gian bảo hành: 36 tháng</span></span>
                                                        </li>
                                                    </ul>

                                                    <p style="text-align:justify"><span
                                                                style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px">Nhanh chóng đặt mua chiếc vòi chậu Belli BL1140 vì sức khỏe của bạn và gia đình. Sản phẩm phân phối độc quyền tại đại lý Hải Linh. </span></span>
                                                    </p>

                                                    <p style="text-align:justify"><span
                                                                style="font-family:Arial,Helvetica,sans-serif"><span
                                                                    style="font-size:14px">Quý khách hàng Click mua nhanh hoặc liên hệ hotline đặt hàng nhanh chóng. Hoặc quý khách có thể đến trực tiếp Showroom Hải Linh để tìm hiểu sản phẩm và mua sắm. Đại lý Hải Linh rất hân hạnh được phục vụ và cam kết mang lại lợi ích tối đa cho khách hàng.&nbsp;</span></span>
                                                    </p></div>
                                            </div>
                                            <div class="readmore" id="readmore_desc">
                                                <span class="closed">Xem thêm</span></div>
                                            <div class="readmore hide" id="readany_desc">
                                                <span class="closed">Rút gọn</span></div>
                                            <div class="product_tags"></div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="frame_b_r">
                            <div class="tab_content_right">
                                <div class="characteristic">
                                    <div class="title-box-product">Thông số kỹ thuật</div>
                                    <table class="charactestic_table" border="0" bordercolor="#EEE" cellpadding="7" width="100%">
                                        <tbody>
                                        <tr class="tr-0">
                                            <td class="title_charactestic" width="40%">Loại vòi chậu</td>
                                            <td class="content_charactestic"> Vòi 1 lỗ,																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														</td>
                                        </tr>
                                        <tr class="tr-1">
                                            <td class="title_charactestic" width="40%"> Xuất xứ</td>
                                            <td class="content_charactestic">Việt Nam</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="products-list-related">
                            <div class="tab-title cls">
                                <div class="cat-title-main" id="characteristic-label">
                                    <span>Sản phẩm liên quan</span>
                                </div>
                                <div class="product_grid product_grid_5 product_grid_slide owl-carousel owl-theme owl-responsive-1170">
                                    <div class="item">
                                        <div class="frame_inner">
                                            <figure class="product_image ">
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205">
                                                    <img class="lazy after-lazy" alt="Vòi chậu Inax LFV-1102SP-1" src="https://hailinh.vn/images/products/2020/11/23/resized/voi-chau-inax-lfv-1102sp-1_1606113720.jpg">
                                                </a>
                                            </figure>
                                            <h3>
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205" class="name">
                                                    Vòi chậu Belli BL3205
                                                </a>
                                            </h3>
                                            <div class="price_arae">
                                                <span class="price_current">1.460.000₫</span>
                                                <span class="price_old">2.090.000₫</span>
                                                <span class="discount_tt">(-30%)</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="frame_inner">
                                            <figure class="product_image ">
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205">
                                                    <img class="lazy after-lazy" alt="Vòi chậu Inax LFV-1102SP-1" src="https://hailinh.vn/images/products/2020/11/23/resized/voi-chau-inax-lfv-1102sp-1_1606113720.jpg">
                                                </a>
                                            </figure>
                                            <h3>
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205" class="name">
                                                    Vòi chậu Belli BL3205
                                                </a>
                                            </h3>
                                            <div class="price_arae">
                                                <span class="price_current">1.460.000₫</span>
                                                <span class="price_old">2.090.000₫</span>
                                                <span class="discount_tt">(-30%)</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="frame_inner">
                                            <figure class="product_image ">
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205">
                                                    <img class="lazy after-lazy" alt="Vòi chậu Inax LFV-1102SP-1" src="https://hailinh.vn/images/products/2020/11/23/resized/voi-chau-inax-lfv-1102sp-1_1606113720.jpg">
                                                </a>
                                            </figure>
                                            <h3>
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205" class="name">
                                                    Vòi chậu Belli BL3205
                                                </a>
                                            </h3>
                                            <div class="price_arae">
                                                <span class="price_current">1.460.000₫</span>
                                                <span class="price_old">2.090.000₫</span>
                                                <span class="discount_tt">(-30%)</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="frame_inner">
                                            <figure class="product_image ">
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205">
                                                    <img class="lazy after-lazy" alt="Vòi chậu Inax LFV-1102SP-1" src="https://hailinh.vn/images/products/2020/11/23/resized/voi-chau-inax-lfv-1102sp-1_1606113720.jpg">
                                                </a>
                                            </figure>
                                            <h3>
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205" class="name">
                                                    Vòi chậu Belli BL3205
                                                </a>
                                            </h3>
                                            <div class="price_arae">
                                                <span class="price_current">1.460.000₫</span>
                                                <span class="price_old">2.090.000₫</span>
                                                <span class="discount_tt">(-30%)</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="frame_inner">
                                            <figure class="product_image ">
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205">
                                                    <img class="lazy after-lazy" alt="Vòi chậu Inax LFV-1102SP-1" src="https://hailinh.vn/images/products/2020/11/23/resized/voi-chau-inax-lfv-1102sp-1_1606113720.jpg">
                                                </a>
                                            </figure>
                                            <h3>
                                                <a href="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl3205-p7867.html" title="Vòi chậu Belli BL3205" class="name">
                                                    Vòi chậu Belli BL3205
                                                </a>
                                            </h3>
                                            <div class="price_arae">
                                                <span class="price_current">1.460.000₫</span>
                                                <span class="price_old">2.090.000₫</span>
                                                <span class="discount_tt">(-30%)</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rate-comment-plugin">
                            <div class="tab-title cls">
                                <div class="cat-title-main" id="tab-title-label">
                                    <span>Đánh giá - Bình luận</span>
                                </div>
                            </div>
                            <div id="prodetails_tab3" class="prodetails_tab">
                                <div class="tab_content_right">
                                    <script>
                                        function load_ajax_paginationrate($value){
                                            $.get($value,{}, function(html){
                                                $('#_info_rate').html(html);
                                                $('html, body').animate({scrollTop:$('#_info_rate').position().top}, 'slow');
                                            });
                                        }
                                    </script>
                                    <div class="full-screen-mobile"></div>
                                    <div id="rates_rate">
                                        <div class="rates">
                                            <div class="tab_label">
                                                <span>Có <strong>0</strong> đánh giá</span><strong>về Vòi chậu Belli BL1140</strong>
                                            </div>
                                            <div class="toprt">
                                                <div class="crt">
                                                    <div class="rcrt">
                                                        <div class="r">
                                                            <span class="t">5 <i></i></span>
                                                            <div class="bgb">
                                                                <div class="bgb-in" style="width: 0%"></div>
                                                            </div>
                                                            <span class="n" onclick="" data-buy="2"><strong>0</strong> đánh giá</span>
                                                        </div>

                                                        <div class="r">
                                                            <span class="t">4 <i></i></span>
                                                            <div class="bgb">

                                                                <div class="bgb-in" style="width: 0%"></div>
                                                            </div>
                                                            <span class="n" onclick="" data-buy="0"><strong>0</strong> đánh giá</span>
                                                        </div>

                                                        <div class="r">
                                                            <span class="t">3 <i></i></span>
                                                            <div class="bgb">
                                                                <div class="bgb-in" style="width: 0%"></div>
                                                            </div>
                                                            <span class="n" onclick="" data-buy="0"><strong>0</strong> đánh giá</span>
                                                        </div>

                                                        <div class="r">
                                                            <span class="t">2 <i></i></span>
                                                            <div class="bgb">
                                                                <div class="bgb-in" style="width: 0%"></div>
                                                            </div>
                                                            <span class="n" onclick="" data-buy="0"><strong>0</strong> đánh giá</span>
                                                        </div>

                                                        <div class="r">
                                                            <span class="t">1 <i></i></span>
                                                            <div class="bgb">
                                                                <div class="bgb-in" style="width: 0%"></div>
                                                            </div>
                                                            <span class="n" onclick="" data-buy="0"><strong>0</strong> đánh giá</span>
                                                        </div>

                                                    </div>
                                                    <div class="bcrt">
                                                        <a href="javascript:showInputRating()" class="">Gửi đánh giá của bạn</a>
                                                    </div>
                                                </div>
                                                <div class="clr"></div>
                                                <form action="javascript:void(0);" method="post" name="rate_add_form" id="rate_add_form" class="form_rate hide_form cls" onsubmit="javascript: submit_rate();return false;" style="display: none;">
                                                    <div class="rating_area cls">
                                                        <span class="label_form">Chọn đánh giá của bạn</span>
                                                        <span id="ratings" class="cls">
													        <i class="icon_v1 star_on" id="rate_1" value="1"></i>
												            <i class="icon_v1 star_on" id="rate_2" value="2"></i>
												            <i class="icon_v1 star_on" id="rate_3" value="3"></i>
												            <i class="icon_v1 star_on" id="rate_4" value="4"></i>
												            <i class="icon_v1 star_on" id="rate_5" value="5"></i>
                                                            <input type="hidden" name="rating_disable" id="rating_disable" value="0">
                                                            <input type="hidden" name="rating_value" id="rating_value" value="5">
                                                        </span>
                                                    <span class="rsStar">Tuyệt vời quá</span>
                                                    </div>
                                                    <div class="wraper_form_rate">
                                                        <div class="_textarea">
                                                            <textarea name="content" id="rate_content" placeholder="Nhập đánh giá về sản phẩm(Tối thiểu 30 ký tự)" onkeyup="countTxtRating()"></textarea>
                                                            <div class="extCt hide">
                                                                <span class="ckt"></span>
                                                            </div>
                                                        </div>
                                                        <!-- <input type="button" class="btn-rate-mb" value="Gửi bình luận">   -->
                                                        <div class="wrap_rate cls">
                                                            <div class="wrap_loginpost">
                                                                <aside class="_right">
                                                                    <div>
                                                                        <input class="txt_input" required="" name="name" type="text" placeholder="Họ tên (bắt buộc)" id="rate_name" autocomplete="off" value="">
                                                                    </div>
                                                                    <div>
                                                                        <input class="txt_input" required="" name="email" type="email" placeholder="Email (bắt buộc)" id="rate_email" value="">
                                                                    </div>
                                                                    <div class="wrap_submit mbl">
                                                                        <div class="pull-right clearfix">
                                                                            <input type="submit" class="_btn_rate" value="Gửi đánh giá">
                                                                        </div>
                                                                    </div>
                                                                </aside>
                                                            </div>
                                                        </div>
                                                        <div class="MsgRt"></div>
                                                        <input type="hidden" value="rates" name="module">
                                                        <input type="hidden" value="rates" name="view">
                                                        <input type="hidden" value="products" name="type" id="_rate_type">
                                                        <input type="hidden" value="7874" name="record_id" id="record_id">
                                                        <input type="hidden" value="products" name="_rate_module" id="_rate_module">
                                                        <input type="hidden" value="product" name="_rate_view" id="_rate_view">
                                                        <input type="hidden" value="save_rate" name="task">
                                                        <input type="hidden" value="7874" name="record_id" id="_rate_record_id">
                                                        <input type="hidden" value="L3ZvaS1jaGF1LW5vbmctbGFuaC92b2ktY2hhdS1iZWxsaS1ibDExNDAtcDc4NzQuaHRtbA==" name="return" id="_rate_return">
                                                        <input type="hidden" name="linkurlall" id="linkurlall" value="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl1140-p7874.html">
                                                        <input type="hidden" value="/index.php?module=rates&amp;view=rates&amp;type=products&amp;task=save_rate&amp;raw=1" name="return" id="link_reply_rate">
                                                    </div>
                                                </form>
                                            </div>
                                            <div id="_info_rate" class="cls"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="prodetails_tab20" class="prodetails_tab">
                                <div class="tab_content_right">
                                    <div class="comments">
                                        <div class="tab_label"><span>Có <strong>0</strong> bình luận, đánh giá</span> <strong>về Vòi chậu Belli BL1140</strong> </div>
                                        <form method="post" class="comment_keyword_wrapper" onsubmit="return search_comment();">
                                            <input type="text" id="comment_keyword" name="comment_keyword" placeholder="Tìm theo nội dung, người gửi...">
                                            <button type="submit" class="button-search button">
                                                <svg aria-hidden="true" data-prefix="far" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16">
                                                    <path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1
                                                        322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7
                                                        12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6
                                                        160 160-71.6 160-160 160z" class="">
                                                    </path>
                                                </svg>
                                            </button>
                                        </form>
                                        <div id="_info_comment" class="cls">
                                            <div class="_contents">
                                                <div class="_item 1 _level_0 _sub_0">
                                                    <article itemprop="review" itemscope="" itemtype="http://schema.org/Review">
                                                        <p class="clearfix cls" itemprop="author" itemtype="http://schema.org/Person">
                                                            <span class="_avatar">TV</span><span class="_name" itemprop="name">Quản trị viên</span>
                                                            <span class="_level">Quản trị viên</span>
                                                        </p>
                                                        <div class="_wrap">
                                                            <p class="_content " itemprop="description"> Xin chào quý khách. Quý khách hãy để lại bình luận, chúng tôi sẽ phản hồi sớm</p>
                                                            <div class="_control">
                                                                <a class="button_reply" href="javascript: void(0)">Trả lời</a>
                                                                <span class="dot">.</span>
                                                                <time itemprop="datePublished" content="2020-09-28 00:00" datetime="2020-09-28 00:00" title="2020-09-28 00:00">7 tháng trước</time>
                                                            </div>
                                                            <div class="reply_area hide">
                                                                <form action="javascript:void(0);" method="post" name="comment_reply_form_1" id="comment_reply_form_1" class="form_comment cls" onsubmit="javascript: submit_reply(1);return false;">
                                                                    <div class="_textarea">
                                                                        <textarea texid="txt_content_1" id="cmt_content_1" class="cmt_content" name="content" placeholder="Viết bình luận của bạn..."></textarea>
                                                                    </div
                                                                    <input type="button" class="btn-comment-mb-rep" value="Gửi bình luận"><div class="full-screen-mobile">
                                                                    </div>
                                                                    <div class="wrap_r cls">
                                                                        <div class="title-mb">Thông tin người gửi
                                                                            <span class="close-md-comment">
                                                                                <svg height="16px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
                                                                                      <g>
                                                                                        <path fill="black" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59
                                                                                            c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59
                                                                                            c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0
                                                                                            L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z">
                                                                                        </path>
                                                                                      </g>
                                                                                    </svg>
                                                                            </span>
                                                                        </div>
                                                                        <div class="wrap_loginpost mbl">
                                                                            <aside class="_right">
                                                                                <input required="" class="txt_input" name="name" id="cmt_name_1" type="text" placeholder="Họ tên (bắt buộc)" maxlength="50" autocomplete="off" value="">
                                                                                <input required="" class="txt_input" name="email" id="cmt_email_1" type="email" placeholder="Email (bắt buộc)" value="">
                                                                            </aside>
                                                                        </div>
                                                                        <div class="wrap_submit mbl">
                                                                            <input type="submit" class="_btn_comment" value="Gửi bình luận">
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" value="products" name="module" id="_cmt_module_1"><input type="hidden" value="product" name="view" id="_cmt_view_1"><input type="hidden" value="products" name="type" id="_cmt_type_1">
                                                                    <input type="hidden" value="save_reply" name="task"><input type="hidden" value="1" name="parent_id" id="cmt_parent_id_1"><input type="hidden" value="0" name="record_id" id="_cmt_record_id_1">
                                                                    <input type="hidden" value="" name="return" id="_cmt_return_1"><input type="hidden" value="/index.php?module=comments&amp;view=comments&amp;type=products&amp;task=save_reply&amp;raw=1" name="return" id="cmt_link_reply_form_1">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="javascript:void(0);" method="post" name="comment_add_form" id="comment_add_form" class="form_comment cls" onsubmit="javascript: submit_comment();return false;">
                                            <label class="label_form">Bình luận</label>
                                            <div class="_textarea">
                                                <textarea name="content" id="cmt_content" placeholder="Viết bình luận của bạn..."></textarea>
                                            </div>
                                            <button type="button" class="btn-comment-mb">Gửi bình luận  </button>
                                            <div class="wrap_r cls">
                                                <div class="title-mb">
                                                    Thông tin người gửi
                                                    <span class="close-md-comment">
                                                        <svg height="16px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
                                                          <g>
                                                            <path fill="black" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59
                                                                c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59
                                                                c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0
                                                                L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z">
                                                            </path>
                                                          </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="wrap_loginpost">
                                                    <aside class="_right">
                                                        <div>
                                                            <input class="txt_input" required="" name="name" type="text" placeholder="Họ tên (bắt buộc)" id="cmt_name" autocomplete="off" value="">
                                                        </div>
                                                        <div>
                                                            <input class="txt_input" required="" name="email" type="email" placeholder="Email (bắt buộc)" id="cmt_email" value="">
                                                        </div>

                                                    </aside>
                                                </div>
                                                <div class="wrap_submit mbl">
                                                    <div class="pull-right clearfix">
                                                        <input type="submit" class="_btn_comment" value="Gửi bình luận">
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" value="comments" name="module">
                                            <input type="hidden" value="comments" name="view">
                                            <input type="hidden" value="products" name="type" id="_cmt_type">
                                            <input type="hidden" value="products" name="_cmt_module" id="_cmt_module">
                                            <input type="hidden" value="product" name="_cmt_view" id="_cmt_view">
                                            <input type="hidden" value="save_comment" name="task">
                                            <input type="hidden" value="7874" name="record_id" id="_cmt_record_id">
                                            <input type="hidden" value="" name="return" id="_cmt_return">
                                            <input type="hidden" name="linkurlall" id="cmt_linkurlall" value="https://hailinh.vn/voi-chau-nong-lanh/voi-chau-belli-bl1140-p7874.html">
                                            <input type="hidden" value="/index.php?module=comments&amp;view=comments&amp;type=products&amp;task=save_comment&amp;raw=1" name="return" id="link_reply_form">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="pos4 container">
        <div class="block_strengths strengths-_strengths strengths_0 block" id="block_id_132">
            <p class="block_title"><span>Hải Linh - Nơi hội tụ của các thương hiệu nổi tiếng</span></p>
            <div class="container">
                <div class="block-strengths block-strengths-2 block-strengths-row-2">
                    <div class="item">
                        <a href="#" alt="Cam kết bán hàng chính hãng" class="asvg">
                            <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                    <g>
                                        <path d="M493.563,431.87l-58.716-125.913c-32.421,47.207-83.042,80.822-141.639,91.015l49.152,105.401
                                            c6.284,13.487,25.732,12.587,30.793-1.341l25.193-69.204l5.192-2.421l69.205,25.193
                                            C486.63,459.696,499.839,445.304,493.563,431.87z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M256.001,0C154.815,0,72.485,82.325,72.485,183.516s82.331,183.516,183.516,183.516
                                            c101.186,0,183.516-82.325,183.516-183.516S357.188,0,256.001,0z M345.295,170.032l-32.541,31.722l7.69,44.804
                                            c2.351,13.679-12.062,23.956-24.211,17.585l-40.231-21.148l-40.231,21.147c-12.219,6.416-26.549-3.982-24.211-17.585l7.69-44.804
                                            l-32.541-31.722c-9.89-9.642-4.401-26.473,9.245-28.456l44.977-6.533l20.116-40.753c6.087-12.376,23.819-12.387,29.913,0
                                            l20.116,40.753l44.977,6.533C349.697,143.557,355.185,160.389,345.295,170.032z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M77.156,305.957L18.44,431.87c-6.305,13.497,7.023,27.81,20.821,22.727l69.204-25.193l5.192,2.421l25.193,69.205
                                            c5.051,13.899,24.496,14.857,30.793,1.342l49.152-105.401C160.198,386.779,109.578,353.165,77.156,305.957z">
                                        </path>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="#" alt="Cam kết bán hàng chính hãng">Cam kết bán hàng chính hãng</a>
                        </div>
                    </div>
                    <div class="item">
                        <a href="#" alt="14 năm kinh nghiệm" class="asvg">
                            <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                    <g>
                                        <path d="M122.763,306.519V156.766c0-11.732,3.094-22.747,8.488-32.3c-12.26,0-85.918,0-98.238,0v0.006
                                            c-17.906,0.124-32.385,14.674-32.385,32.609v149.752c0,10.118,8.202,18.32,18.32,18.32h23.826l6.646,136.613
                                            c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858l5.217-107.249
                                            C137.304,348.68,122.763,328.929,122.763,306.519z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <circle cx="412.182" cy="52.901" r="52.274"></circle>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M480.467,124.825l0.002-0.044c-8.09,0-81.852,0-99.539,0c5.284,9.48,8.307,20.384,8.307,31.987V306.52
                                            c0,22.085-14.07,41.323-34.134,48.572l5.205,106.989c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858
                                            l6.697-137.674h22.346c10.118,0,18.32-8.202,18.32-18.32V157.395C511.373,139.955,497.685,125.714,480.467,124.825z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M325.024,124.196l0.002-0.044h-51.693l10.126,10.126c2.379,2.379,2.379,6.238,0,8.617l-10.678,10.678l8.313,93.003
                                            c0.288,3.217-0.578,6.431-2.445,9.067l-17.674,24.973c-1.143,1.614-2.997,2.574-4.973,2.574c-1.976,0-3.832-0.959-4.973-2.574
                                            l-17.674-24.973c-1.865-2.636-2.732-5.85-2.445-9.067l8.313-93.003l-10.678-10.678c-2.379-2.379-2.379-6.238,0-8.617
                                            l10.126-10.126h-50.213v0.006c-17.906,0.124-32.384,14.673-32.384,32.609v149.752c0,10.118,8.202,18.32,18.32,18.32h23.826
                                            l1.62,33.309l5.025,103.304c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858l5.077-104.366
                                            l1.62-33.309h22.346c10.118,0,18.32-8.202,18.32-18.32V156.766h0.001C355.929,139.327,342.241,125.086,325.024,124.196z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path
                                                d="M256.741,0c-28.869,0-52.272,23.403-52.272,52.272c0,28.863,23.41,52.274,52.272,52.274    c28.866,0,52.272-23.414,52.272-52.272C309.013,23.403,285.61,0,256.741,0z"></path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <circle cx="101.294" cy="52.584" r="52.274"></circle>
                                    </g>
                                </g> </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="https://www.youtube.com/watch?v=Q9NMzGroYlk&amp;feature=emb_logo"
                               alt="14 năm kinh nghiệm">14 năm kinh nghiệm</a>

                        </div>
                    </div>
                    <div class="item">
                        <a href="#" alt="Đối tác của các thương hiệu hàng đầu" class="asvg">
                            <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512">
                                <g>
                                    <path d="m386.158 8.733c-5.858-5.858-15.355-5.858-21.213 0l-36.478 36.478c-19.923-10.769-45.386-7.748-62.199
                                         9.065l-69.416 69.416c-14.041 14.041-14.041 36.806 0 50.846 14.041 14.041 36.806 14.041 50.846 0l25.09-25.09c38.912
                                         32.448 92.273 42.6 140.591 26.252l66.82 66.82c8.376-18.192 5.834-40.216-7.65-56.069l35.058-35.058c5.858-5.858 5.858-15.355
                                         0-21.213-.101-.101-.211-.187-.315-.284l.015-.015z">
                                    </path>
                                    <path d="m276.459 400.011c-14.041-14.041-36.806-14.041-50.847 0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.847
                                        0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.846 0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.847
                                        0l-46.272 46.272c-14.041 14.041-14.041 36.806 0 50.847 14.041 14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.846 14.041
                                        14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.847 14.041 14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.846
                                        14.041 14.041 36.806 14.041 50.846 0l46.272-46.272c14.044-14.043 14.044-36.807.003-50.848z">
                                    </path>
                                    <path d="m276.16 188.504-7.248 7.248c-25.715 25.715-67.558 25.715-93.273 0s-25.715-67.558 0-93.273l57.514-57.514c-16.157-6.188-34.547-4.891-49.768
                                        3.892l-36.329-36.329c-5.858-5.858-15.355-5.858-21.213 0l-121.45 121.449c-5.858 5.858-5.858 15.355 0 21.213l36.329 36.329c-8.783 15.221-10.08 33.611-3.892
                                        49.768l15.029-15.029c25.715-25.715 67.558-25.715 93.273 0 9.374 9.374 15.315 20.895 17.854 32.992 12.098 2.54 23.618 8.48 32.992 17.854s15.314
                                        20.895 17.854 32.993c12.098 2.54 23.618 8.48 32.992 17.853 9.374 9.374 15.315 20.895 17.854 32.992 12.098 2.54 23.618 8.48 32.992 17.854 25.715
                                        25.715 25.715 67.558 0 93.273l-18.562 18.562 6.497 6.497c14.041 14.041 36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041
                                        36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041 36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041
                                        36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847l-84.475-84.475c-43.996 9.707-89.992 2.097-128.358-20.761z">
                                    </path>
                                </g>
                            </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="https://hailinh.vn/thuong-hieu.html"
                               alt="Đối tác của các thương hiệu hàng đầu">Đối tác của các thương hiệu hàng đầu
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include 'footer.php';
?>