<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>hailinh.com</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<header>
    <div class="header_wrapper_wrap">
        <div class="header_wrapper header_wrapper_img">
            <div class="header container cls" id="header_inner">
                <div class="header-l">
                    <div class="sb-toggle-left navbar-left menu_show" id="click_menu_mobile_code">
                        <a href="javascript:void(0)" class="all-navicon-line">
                            <div class="navicon-line navicon-line-1"></div>
                            <div class="navicon-line navicon-line-2"></div>
                            <div class="navicon-line navicon-line-3"></div>
                        </a>
                    </div>


                    <h1><a href="https://hailinh.vn/" title="Hải Linh" class="logo" rel="home">
                        <img width="190" height="68" class="logo_img"
                             src="https://hailinh.vn/images/config/logo-hai-linh_to-011111111111111111_1614325031.png"
                             alt="Hải Linh" title="" style="">

                        <img width="60" height="57" class="logo_img_small"
                             src="https://hailinh.vn/images/config/logo_mobile_1592897490_1595648177.png" alt="Hải Linh">
                    </a>
                    </h1>
                    <div class="regions_search cls">
                        <div id="search" class="search search-contain s_close">
                            <div class="search-content">
                                <form action="https://hailinh.vn/tim-kiem/cat-all/manf-all.html" name="search_form"
                                      id="search_form" method="get"
                                      onsubmit="javascript: submit_form_search();return false;">
                                    <input type="text" value="" aria-label="Tìm kiếm sản phẩm"
                                           placeholder="Tiết kiệm 50% khi giao hàng! Cơ hội cuối cùng!" id="keyword"
                                           name="keyword" class="keyword input-text" autocomplete="off">

                                    <button type="submit" class="button-search button" aria-label="tìm kiếm">
                                        <span class="icon-search"></span>
                                    </button>

                                    <input type="hidden" name="module" value="news">
                                    <input type="hidden" name="module" id="link_search"
                                           value="https://hailinh.vn/tim-kiem/keyword/cat-all/manf-all.html">
                                    <input type="hidden" name="view" value="search">
                                    <input type="hidden" name="Itemid" value="10">
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="header-r cls">
                    <div class="support_top">
                        <span class="icon-question"></span>
                        <a href="https://hailinh.vn/hoi-dap.html" title="Hỗ trợ">Hỗ trợ</a>
                    </div>
                    <div class="shopcart">
                        <div class="shopcart_simple block_content">
                            <a class="buy_icon" href="https://hailinh.vn/gio-hang.html" title="Giỏ hàng" rel="nofollow">
                                <span class="icon-shopping-cart"></span>
                                <span class="text-c">Giỏ hàng</span>
                                <span class="quality">0</span>
                            </a>
                        </div>
                    </div>
                    <div class="login_icon" onclick="OpenLoginPopup()">
                        <a id="log-mobile" class="login_icon_inner" href="javascript:void(0)" title="Đăng nhập - Đăng ký">
                            <span class="icon-user"></span>
                            <span>Tài khoản</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="div_megamenu">
            <div class="container cls">
                <div class="div_megamenu_left">
                    <div class="menu_show menu_label product_menu_normal active" data-id="product_menu_ul" id="click_menu_mobile_code">
                        <span>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 455 455" style="enable-background:new 0 0 455 455;" xml:space="preserve">
                                <g fill="#FC8600">
                                    <rect y="312.5" width="455" height="30" fill="#FC8600"></rect>
                                    <rect y="212.5" width="455" height="30" fill="#FC8600"></rect>
                                    <rect y="112.5" width="455" height="30" fill="#FC8600"></rect>
                                </g>
                            </svg>
                            Danh mục<font> sản phẩm</font>
                        </span>
                    </div>
                    <div class="product_menu_fix_top">

                        <div class="product_menu " id="product_menu_top">

                            <div id="product_menu_ul" class="menu bl">
                                <ul class="product_menu_ul_innner scroll-bar">
                                    <!--	LEVEL 0			-->
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1036">
                                        <a href="https://hailinh.vn/gach-op-lat-pc83.html" id="menu_item_1036"
                                           class="menu_item_a" title="Gạch ốp lát">
                                            <span class="text-menu">Gạch ốp lát</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284
                                                 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495
                                                 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="javascript:void(0)" title="Thương hiệu" class="name">
                                                            Thương hiệu </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/gach-op-lat-viglacera-pcm83.html"
                                                               title="Gạch Viglacera">Gạch Viglacera</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-taicera-pcm83.html"
                                                               title="Gạch Taicera">Gạch Taicera</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-dong-tam-pcm83.html"
                                                               title="Gạch Đồng Tâm">Gạch Đồng Tâm</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-mosaic-pcm83.html"
                                                               title="Gạch Mosaic">Gạch Mosaic</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-tay-ban-nha-pcm83.html"
                                                               title="Gạch Tây Ban Nha">Gạch Tây Ban Nha</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-an-do-pcm83.html"
                                                               title="Gạch Ấn Độ">Gạch Ấn Độ</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-y-my-pcm83.html"
                                                               title="Gạch Ý Mỹ">Gạch Ý Mỹ</a>
                                                            <a href="https://hailinh.vn/gach-cnc-pc344.html"
                                                               title="Gạch CNC">Gạch CNC</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/gach-op-tuong-pc150.html"
                                                           title="Gạch ốp tường" class="name">
                                                            Gạch ốp tường </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/gach-op-tuong-12x36-pc230.html"
                                                               title="Gạch ốp tường 12x36">Gạch ốp tường 12x36</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-15x45-pc227.html"
                                                               title="Gạch ốp tường 15x45">Gạch ốp tường 15x45</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-15x60-pc242.html"
                                                               title="Gạch ốp tường 15x60">Gạch ốp tường 15x60</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-20x80-pc167.html"
                                                               title="Gạch ốp tường 20x80">Gạch ốp tường 20x80</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-30x30-pc221.html"
                                                               title="Gạch ốp tường 30x30">Gạch ốp tường 30x30</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-30x60-pc161.html"
                                                               title="Gạch ốp tường 30x60">Gạch ốp tường 30x60</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-40x80-pc152.html"
                                                               title="Gạch ốp tường 40x80">Gạch ốp tường 40x80</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-30x90-pc273.html"
                                                               title="Gạch ốp tường 30x90">Gạch ốp tường 30x90</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-20x20-pc348.html"
                                                               title="Gạch ốp tường 20x20">Gạch ốp tường 20x20</a>
                                                            <a href="https://hailinh.vn/gach-op-tuong-kich-thuoc-khac-pc164.html"
                                                               title="Kích thước khác">Kích thước khác</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/gach-lat-nen-pc151.html"
                                                           title="Gạch lát nền" class="name">
                                                            Gạch lát nền </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/gach-lat-nen-30x30-pc154.html"
                                                               title="Gạch lát nền 30x30">Gạch lát nền 30x30</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-40x40-pc169.html"
                                                               title="Gạch lát nền 40x40">Gạch lát nền 40x40</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-50x50-pc171.html"
                                                               title="Gạch lát nền 50x50">Gạch lát nền 50x50</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-60x60-pc156.html"
                                                               title="Gạch lát nền 60x60">Gạch lát nền 60x60</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-75x75-pc157.html"
                                                               title="Gạch lát nền 75x75">Gạch lát nền 75x75</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-80x80-pc158.html"
                                                               title="Gạch lát nền 80x80">Gạch lát nền 80x80</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-30x60-pc165.html"
                                                               title="Gạch lát nền 30x60">Gạch lát nền 30x60</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-60x120-pc155.html"
                                                               title="Gạch lát nền 60x120">Gạch lát nền 60x120</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-20x20-pc350.html"
                                                               title="Gạch lát nền 20x20">Gạch lát nền 20x20</a>
                                                            <a href="https://hailinh.vn/gach-lat-nen-kich-thuoc-khac-pc160.html"
                                                               title="Kích thước khác">Kích thước khác</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                    <div class="col">
                                                        <a href="javascript:void(0)" title="Bộ sưu tập" class="name">
                                                            Bộ sưu tập </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-trang-tri-dlt.html"
                                                               title="Gạch trang trí">Gạch trang trí</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-van-da-dlt.html"
                                                               title="Gạch vân đá">Gạch vân đá</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-van-may-dlt.html"
                                                               title="Gạch vân mây">Gạch vân mây</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-gia-xi-mang-dlt.html"
                                                               title="Gạch giả xi măng">Gạch giả xi măng</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-mot-mau-dlt.html"
                                                               title="Gạch một màu">Gạch một màu</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-bong-dlt.html"
                                                               title="Gạch bông">Gạch bông</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-5">
                                                    <div class="col">
                                                        <a href="javascript:void(0)" title="Công năng gạch" class="name">
                                                            Công năng gạch </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-op-lat-ngoai-troi-dlt.html"
                                                               title="Gạch ốp lát ngoài trời">Gạch ốp lát ngoài trời</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-op-lat-trong-nha-dlt.html"
                                                               title="Gạch ốp lát trong nhà">Gạch ốp lát trong nhà</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-op-lat-be-boi-dlt.html"
                                                               title="Gạch ốp bể bơi">Gạch ốp bể bơi</a>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="javascript:void(0)" title="Xuất xứ" class="name">
                                                            Xuất xứ </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-nhap-khau-dlt.html"
                                                               title="Gạch nhập khẩu">Gạch nhập khẩu</a>
                                                            <a href="https://hailinh.vn/gach-op-lat-pc83/gach-trong-nuoc-dlt.html"
                                                               title="Việt Nam">Việt Nam</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1302">
                                        <a href="https://hailinh.vn//ngoi-lop-mai-nha-pc352.html" id="menu_item_1302"
                                           class="menu_item_a" title="Ngói lợp mái nhà">
                                            <span class="text-menu">Ngói lợp mái nhà</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686
                                                 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284
                                                 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="javascript:void(0)" title="Thương hiệu" class="name">
                                                            Thương hiệu </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-viglacera-pcm352.html"
                                                               title="Viglacera">Viglacera</a>
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-dat-viet-pcm352.html"
                                                               title="Đất Việt">Đất Việt</a>
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-prime-pcm352.html"
                                                               title="Prime">Prime</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="javascript:void(0)" title="Kiểu ngói" class="name">
                                                            Kiểu ngói </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-phang-dlt.html"
                                                               title="Ngói phẳng">Ngói phẳng</a>
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-song-dlt.html"
                                                               title="Ngói sóng">Ngói sóng</a>
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-mui-dlt.html"
                                                               title="Ngói mũi">Ngói mũi</a>
                                                            <a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-noc-dlt.html"
                                                               title="Ngói nóc">Ngói nóc</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                </div>
                                                <div class="col-number col-4">
                                                </div>
                                                <div class="col-number col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1037">
                                        <a href="https://hailinh.vn/thiet-bi-ve-sinh-pc73.html" id="menu_item_1037"
                                           class="menu_item_a" title="Thiết bị vệ sinh">
                                            <span class="text-menu">Thiết bị vệ sinh</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686 12.284
                                                 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495
                                                 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bon-cau-pc74.html" title="Bồn cầu"
                                                           class="name">
                                                            Bồn cầu </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/bon-cau-inax-pcm74.html"
                                                               title="Bồn cầu Inax">Bồn cầu Inax</a>
                                                            <a href="https://hailinh.vn/bon-cau-viglacera-pcm74.html"
                                                               title="Bồn cầu Viglacera">Bồn cầu Viglacera</a>
                                                            <a href="https://hailinh.vn/bon-cau-toto-pcm74.html"
                                                               title="Bồn cầu TOTO">Bồn cầu TOTO</a>
                                                            <a href="https://hailinh.vn/bon-cau-1-khoi-pc99.html"
                                                               title="Bồn cầu 1 khối">Bồn cầu 1 khối</a>
                                                            <a href="https://hailinh.vn/bon-cau-2-khoi-pc100.html"
                                                               title="Bồn cầu 2 khối">Bồn cầu 2 khối</a>
                                                            <a href="https://hailinh.vn/bon-cau-thong-minh-pc102.html"
                                                               title="Bồn cầu thông minh">Bồn cầu thông minh</a>
                                                            <a href="https://hailinh.vn/bon-cau-am-tuong-pc101.html"
                                                               title="Bồn cầu âm tường">Bồn cầu âm tường</a>
                                                            <a href="https://hailinh.vn/nap-bon-cau-pc103.html"
                                                               title="Nắp bồn cầu">Nắp bồn cầu</a>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bon-tieu-pc78.html" title="Bồn tiểu"
                                                           class="name">
                                                            Bồn tiểu </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/bon-tieu-inax-pcm78.html"
                                                               title="Bồn tiểu Inax">Bồn tiểu Inax</a>
                                                            <a href="https://hailinh.vn/bon-tieu-viglacera-pcm78.html"
                                                               title="Bồn tiểu Viglacera">Bồn tiểu Viglacera</a>
                                                            <a href="https://hailinh.vn/bon-tieu-toto-pcm78.html"
                                                               title="Bồn tiểu TOTO">Bồn tiểu TOTO</a>
                                                            <a href="https://hailinh.vn/bon-tieu-nam-pc116.html"
                                                               title="Bồn tiểu nam">Bồn tiểu nam</a>
                                                            <a href="https://hailinh.vn/bon-tieu-nu-pc117.html"
                                                               title="Bồn tiểu nữ">Bồn tiểu nữ</a>
                                                            <a href="https://hailinh.vn/phu-kien-bon-tieu-pc118.html"
                                                               title="Phụ kiện bồn tiểu">Phụ kiện bồn tiểu</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/chau-rua-pc75.html" title="Chậu rửa"
                                                           class="name">
                                                            Chậu rửa </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/chau-rua-inax-pcm75.html"
                                                               title="Chậu rửa Inax">Chậu rửa Inax</a>
                                                            <a href="https://hailinh.vn/chau-rua-viglacera-pcm75.html"
                                                               title="Chậu rửa Viglacera">Chậu rửa Viglacera</a>
                                                            <a href="https://hailinh.vn/chau-rua-toto-pcm75.html"
                                                               title="Chậu rửa TOTO">Chậu rửa TOTO</a>
                                                            <a href="https://hailinh.vn/chau-rua-dat-ban-pc104.html"
                                                               title="Chậu rửa đặt bàn">Chậu rửa đặt bàn</a>
                                                            <a href="https://hailinh.vn/chau-rua-treo-tuong-pc105.html"
                                                               title="Chậu rửa treo tường">Chậu rửa treo tường</a>
                                                            <a href="https://hailinh.vn/bo-tu-chau-pc231.html"
                                                               title="Bộ tủ chậu">Bộ tủ chậu</a>
                                                            <a href="https://hailinh.vn/phu-kien-chau-rua-pc109.html"
                                                               title="Phụ kiện chậu rửa">Phụ kiện chậu rửa</a>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/voi-chau-pc114.html" title="Vòi chậu"
                                                           class="name">
                                                            Vòi chậu </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/voi-chau-belli-pcm114.html"
                                                               title="Vòi chậu Belli">Vòi chậu Belli</a>
                                                            <a href="https://hailinh.vn/voi-chau-inax-pcm114.html"
                                                               title="Vòi chậu Inax">Vòi chậu Inax</a>
                                                            <a href="https://hailinh.vn/voi-chau-viglacera-pcm114.html"
                                                               title="Vòi chậu Viglacera">Vòi chậu Viglacera</a>
                                                            <a href="https://hailinh.vn/voi-chau-toto-pcm114.html"
                                                               title="Vòi chậu TOTO">Vòi chậu TOTO</a>
                                                            <a href="https://hailinh.vn/voi-chau-nong-lanh-pc210.html"
                                                               title="Vòi chậu nóng lanh">Vòi chậu nóng lanh</a>
                                                            <a href="https://hailinh.vn/voi-chau-1-duong-nuoc-pc211.html"
                                                               title="Vòi chậu 1 đường lạnh">Vòi chậu 1 đường lạnh</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bon-tam-pc79.html" title="Bồn tắm"
                                                           class="name">
                                                            Bồn tắm </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/bon-tam-belli-pcm79.html"
                                                               title="Bồn tắm Belli">Bồn tắm Belli</a>
                                                            <a href="https://hailinh.vn/bon-tam-inax-pcm79.html"
                                                               title="Bồn tắm Inax">Bồn tắm Inax</a>
                                                            <a href="https://hailinh.vn/bon-tam-toto-pcm79.html"
                                                               title="Bồn tắm TOTO">Bồn tắm TOTO</a>
                                                            <a href="https://hailinh.vn/bon-tam-thuong-pc119.html"
                                                               title="Bồn tắm thường">Bồn tắm thường</a>
                                                            <a href="https://hailinh.vn/bon-tam-massage-pc122.html"
                                                               title="Bồn tắm massage">Bồn tắm massage</a>
                                                            <a href="https://hailinh.vn/phu-kien-bon-tam-pc124.html"
                                                               title="Phụ kiện bồn tắm">Phụ kiện bồn tắm</a>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/sen-tam-pc76.html" title="Sen tắm"
                                                           class="name">
                                                            Sen tắm </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/sen-tam-belli-pcm76.html"
                                                               title="Sen tắm Belli">Sen tắm Belli</a>
                                                            <a href="https://hailinh.vn/sen-tam-inax-pcm76.html"
                                                               title="Sen tắm Inax">Sen tắm Inax</a>
                                                            <a href="https://hailinh.vn/sen-tam-viglacera-pcm76.html"
                                                               title="Sen tắm Viglacera">Sen tắm Viglacera</a>
                                                            <a href="https://hailinh.vn/sen-tam-thuong-pc110.html"
                                                               title="Sen tắm thường">Sen tắm thường</a>
                                                            <a href="https://hailinh.vn/sen-tam-toto-pcm76.html"
                                                               title="Sen tắm TOTO">Sen tắm TOTO</a>
                                                            <a href="https://hailinh.vn/sen-tam-cay-pc111.html"
                                                               title="Sen tắm cây">Sen tắm cây</a>
                                                            <a href="https://hailinh.vn/sen-tam-am-tuong-pc112.html"
                                                               title="Sen tắm âm tường">Sen tắm âm tường</a>
                                                            <a href="https://hailinh.vn/phu-kien-sen-tam-pc113.html"
                                                               title="Phụ kiện sen tắm">Phụ kiện sen tắm</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phu-kien-nha-tam-pc82.html"
                                                           title="Phụ kiện nhà tắm" class="name">
                                                            Phụ kiện nhà tắm </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/bo-phu-kien-pc143.html"
                                                               title="Bộ phụ kiện">Bộ phụ kiện</a>
                                                            <a href="https://hailinh.vn/guong-pc125.html" title="Gương">Gương</a>
                                                            <a href="https://hailinh.vn/hop-nuoc-hoa-pc141.html"
                                                               title="Hộp nước hoa">Hộp nước hoa</a>
                                                            <a href="https://hailinh.vn/hop-dung-xa-phong-pc208.html"
                                                               title="Hộp đựng xà phòng">Hộp đựng xà phòng</a>
                                                            <a href="https://hailinh.vn/ke-de-do-pc126.html"
                                                               title="Kệ để đồ">Kệ để đồ</a>
                                                            <a href="https://hailinh.vn/lo-giay-pc140.html" title="Lô giấy">Lô
                                                                giấy</a>
                                                            <a href="https://hailinh.vn/may-say-tay-pc139.html"
                                                               title="Máy sấy tay">Máy sấy tay</a>
                                                            <a href="https://hailinh.vn/moc-ao-pc136.html" title="Móc áo">Móc
                                                                áo</a>
                                                            <a href="https://hailinh.vn/voi-xit-pc138.html" title="Vòi xịt">Vòi
                                                                xịt</a>
                                                            <a href="https://hailinh.vn/tay-vin-nha-tam-pc144.html"
                                                               title="Tay vịn nhà tắm">Tay vịn nhà tắm</a>
                                                            <a href="https://hailinh.vn/xi-phong-pc196.html"
                                                               title="Xi phông">Xi phông</a>
                                                            <a href="https://hailinh.vn/thoat-san-pc137.html"
                                                               title="Thoát sàn">Thoát sàn</a>
                                                            <a href="https://hailinh.vn/treo-khan-pc132.html"
                                                               title="Treo khăn">Treo khăn</a>
                                                            <a href="https://hailinh.vn/vach-ngan-pc135.html"
                                                               title="Vách ngăn">Vách ngăn</a>
                                                            <a href="https://hailinh.vn/phu-kien-khac-pc142.html"
                                                               title="Phụ kiện khác">Phụ kiện khác</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-5">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phong-xong-hoi-pc80.html"
                                                           title="Phòng xông hơi" class="name">
                                                            Phòng xông hơi </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/phong-xong-hoi-belli-pcm80.html"
                                                               title="Phòng xông hơi Belli">Phòng xông hơi Belli</a>
                                                            <a href="https://hailinh.vn/phong-xong-hoi-govern-pcm80.html"
                                                               title="Phòng xông hơi Govern">Phòng xông hơi Govern</a>
                                                            <a href="https://hailinh.vn/phong-xong-hoi-nofer-pcm80.html"
                                                               title="Phòng xông hơi Nofer">Phòng xông hơi Nofer</a>
                                                            <a href="https://hailinh.vn/phong-xong-hoi-euroking-pcm80.html"
                                                               title="Phòng xông hơi Euroking">Phòng xông hơi Euroking</a>
                                                            <a href="https://hailinh.vn/phong-xong-hoi-appollo-pcm80.html"
                                                               title="Phòng xông hơi Appollo">Phòng xông hơi Appollo</a>
                                                            <a href="https://hailinh.vn/phong-xong-hoi-pc80.html"
                                                               title="Phòng xông hơi Fantiny">Phòng xông hơi Fantiny</a>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phong-tam-kinh-pc81.html"
                                                           title="Phòng tắm kính" class="name">
                                                            Phòng tắm kính </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/phong-tam-kinh-belli-pcm81.html"
                                                               title="Phòng tắm kính Belli">Phòng tắm kính Belli</a>
                                                            <a href="https://hailinh.vn/phong-tam-kinh-euroking-pcm81.html"
                                                               title="Phòng tắm kính Euroking">Phòng tắm kính Euroking</a>
                                                            <a href="https://hailinh.vn/phong-tam-kinh-caesar-pcm81.html"
                                                               title="Phòng tắm kính Caesar">Phòng tắm kính Caesar</a>
                                                            <a href="https://hailinh.vn/phong-tam-kinh-fendi-pcm81.html"
                                                               title="Phòng tắm kính Fendi">Phòng tắm kính Fendi</a>
                                                            <a href="https://hailinh.vn/phong-tam-kinh-fantiny-pcm81.html"
                                                               title="Phòng tắm kính Fantiny">Phòng tắm kính Fantiny</a>
                                                            <a href="https://hailinh.vn/phong-tam-kinh-govern-pcm81.html"
                                                               title="Phòng tắm kính Govern">Phòng tắm kính Govern</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1039">
                                        <a href="https://hailinh.vn/do-gia-dung-pc181.html" id="menu_item_1039"
                                           class="menu_item_a" title="Đồ gia dụng">
                                            <span class="text-menu">Đồ gia dụng</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256 10.454 451.494c-4.686 4.686-4.686
                                                 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495
                                                 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-xay-sinh-to-pc254.html"
                                                           title="Máy xay sinh tố" class="name">
                                                            Máy xay sinh tố </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/noi-chien-khong-dau-pc256.html"
                                                           title="Nồi Chiên không dầu" class="name">
                                                            Nồi Chiên không dầu </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-loc-khong-khi-pc258.html"
                                                           title="Máy lọc không khí" class="name">
                                                            Máy lọc không khí </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/binh-dun-sieu-toc-pc259.html"
                                                           title="Bình đun siêu tốc" class="name">
                                                            Bình đun siêu tốc </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/lo-vi-song-pc260.html"
                                                           title="Lò vi sóng" class="name">
                                                            Lò vi sóng </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-ep-trai-cay-pc261.html"
                                                           title="Máy ép trái cây" class="name">
                                                            Máy ép trái cây </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/chao-pc267.html" title="Chảo"
                                                           class="name">
                                                            Chảo </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/noi-pc268.html" title="Nồi"
                                                           class="name">
                                                            Nồi </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-pha-cafe-pc269.html"
                                                           title="Máy pha cafe" class="name">
                                                            Máy pha cafe </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-suoi-pc272.html" title="Máy sưởi"
                                                           class="name">
                                                            Máy sưởi </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/am-dun-nuoc-pc302.html"
                                                           title="Ấm đun nước" class="name">
                                                            Ấm đun nước </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phu-kien-nha-bep-pc274.html"
                                                           title="Phụ kiện nhà bếp" class="name">
                                                            Phụ kiện nhà bếp </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/tu-ruou-pc275.html" title="Tủ rượu"
                                                           class="name">
                                                            Tủ rượu </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                </div>
                                                <div class="col-number col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1111">
                                        <a href="https://hailinh.vn/noi-that-pc281.html" id="menu_item_1111"
                                           class="menu_item_a" title="Nội thất">
                                            <span class="text-menu">Nội thất</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947
                                                256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284
                                                4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/san-nhua-pc278.html" title="Sàn nhựa"
                                                           class="name">
                                                            Sàn nhựa </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/san-go-pc282.html" title="Sàn gỗ"
                                                           class="name">
                                                            Sàn gỗ </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/quat-tran-pc294.html" title="Quạt trần"
                                                           class="name">
                                                            Quạt trần </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/khoa-cua-pc303.html" title="Khóa cửa"
                                                           class="name">
                                                            Khóa cửa </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/khoa-dien-tu-pc304.html"
                                                               title="Khóa điện tử">Khóa điện tử</a>
                                                            <a href="https://hailinh.vn//" title="Khóa tay nắm cửa">Khóa tay
                                                                nắm cửa</a>
                                                            <a href="https://hailinh.vn//" title="Phụ kiện khóa">Phụ kiện
                                                                khóa</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phu-kien-cua-pc307.html"
                                                           title="Phụ kiện cửa" class="name">
                                                            Phụ kiện cửa </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/tay-nam-cua-pc308.html"
                                                               title="Tay nắm cửa">Tay nắm cửa</a>
                                                            <a href="https://hailinh.vn/ban-le-cua-pc312.html"
                                                               title="Bản lề cửa">Bản lề cửa</a>
                                                            <a href="https://hailinh.vn/thiet-bi-dong-cua-tu-dong-pc313.html"
                                                               title="Thiết bị đóng cửa tự động">Thiết bị đóng cửa tự
                                                                động</a>
                                                            <a href="https://hailinh.vn/chot-chan-cua-pc314.html"
                                                               title="Chốt chặn cửa">Chốt chặn cửa</a>
                                                            <a href="https://hailinh.vn/phu-kien-cua-khac-pc309.html"
                                                               title="Phụ kiện cửa khác">Phụ kiện cửa khác</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/chan-ga-goi-dem-pc200.html"
                                                           title="Chăn ga gối đệm" class="name">
                                                            Chăn ga gối đệm </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/bo-chan-ga-goi-pc201.html"
                                                               title="Bộ chăn ga gối">Bộ chăn ga gối</a>
                                                            <a href="https://hailinh.vn/chan-ga-goi-dem-everon-pcm200.html"
                                                               title="Chăn ga gối đệm Everon">Chăn ga gối đệm Everon</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                </div>
                                                <div class="col-number col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1110">
                                        <a href="https://hailinh.vn/bon-binh-pc285.html" id="menu_item_1110"
                                           class="menu_item_a" title="Bồn bình">
                                            <span class="text-menu">Bồn bình</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947
                                                256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284
                                                4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/binh-nong-lanh-pc286.html"
                                                           title="Bình nóng lạnh" class="name">
                                                            Bình nóng lạnh </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/truc-tiep-pc287.html"
                                                               title="Bình nóng lạnh trực tiếp">Bình nóng lạnh trực tiếp</a>
                                                            <a href="https://hailinh.vn/gian-tiep-pc288.html"
                                                               title="Bình nóng lạnh gián tiếp">Bình nóng lạnh gián tiếp</a>
                                                            <a href="https://hailinh.vn/binh-nong-lanh-ariston-pcm286.html"
                                                               title="Bình nóng lạnh Ariston">Bình nóng lạnh Ariston</a>
                                                            <a href="https://hailinh.vn/binh-nong-lanh-ferroli-pcm286.html"
                                                               title="Bình nóng lạnh Ferroli">Bình nóng lạnh Ferroli</a>
                                                            <a href="https://hailinh.vn/bon-binh-ferroli-pcm285.html"
                                                               title="Bình nóng lạnh Picenza">Bình nóng lạnh Picenza</a>
                                                            <a href="https://hailinh.vn/binh-nong-lanh-rossi-pcm286.html"
                                                               title="Bình nóng lạnh Rossi">Bình nóng lạnh Rossi</a>
                                                            <a href="https://hailinh.vn/binh-nong-lanh-son-ha-pcm286.html"
                                                               title="Bình nóng lạnh Sơn Hà">Bình nóng lạnh Sơn Hà</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bon-nuoc-pc236.html"
                                                           title="Bồn chứa nước" class="name">
                                                            Bồn chứa nước </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/bon-nuoc-pc236/bon-nuoc-bon-dung-dlt.html"
                                                               title="Bồn đứng">Bồn đứng</a>
                                                            <a href="https://hailinh.vn/bon-nuoc-pc236/bon-nuoc-bon-ngang-dlt.html"
                                                               title="Bồn ngang">Bồn ngang</a>
                                                            <a href="https://hailinh.vn/bon-nuoc-tan-a-pcm236.html"
                                                               title="Bồn nước Tân Á">Bồn nước Tân Á</a>
                                                            <a href="https://hailinh.vn/bon-nuoc-son-ha-pcm236.html"
                                                               title="Bồn nước Sơn Hà">Bồn nước Sơn Hà</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/binh-nuoc-nong-nang-luong-mat-troi-pc289.html"
                                                           title="Bình nước nóng năng lượng mặt trời" class="name">
                                                            Bình nước nóng năng lượng mặt trời </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/binh-nuoc-nong-nang-luong-mat-troi-tan-a-pcm289.html"
                                                               title="Bình nước nóng năng lượng mặt trời Tân Á">Bình nước
                                                                nóng năng lượng mặt trời Tân Á</a>
                                                            <a href="https://hailinh.vn/binh-nuoc-nong-nang-luong-mat-troi-son-ha-pcm289.html"
                                                               title="Bình nước nóng năng lượng mặt trời Sơn Hà">Bình nước
                                                                nóng năng lượng mặt trời Sơn Hà</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                </div>
                                                <div class="col-number col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1142">
                                        <a href="https://hailinh.vn/thiet-bi-nha-bep-pc291.html" id="menu_item_1142"
                                           class="menu_item_a" title="Thiết bị nhà bếp">
                                            <span class="text-menu">Thiết bị nhà bếp</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947 256
                                                10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97
                                                0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bep-tu-pc245.html" title="Bếp từ"
                                                           class="name">
                                                            Bếp từ </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bep-dien-tu-pc249.html"
                                                           title="Bếp điện từ" class="name">
                                                            Bếp điện từ </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/bep-ga-pc255.html" title="Bếp ga"
                                                           class="name">
                                                            Bếp ga </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/lo-nuong-pc257.html" title="Lò nướng"
                                                           class="name">
                                                            Lò nướng </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-hut-mui-pc266.html"
                                                           title="Máy hút mùi" class="name">
                                                            Máy hút mùi </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/voi-bep-pc115.html" title="Vòi bếp"
                                                           class="name">
                                                            Vòi bếp </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-rua-bat-pc270.html"
                                                           title="Máy rửa bát" class="name">
                                                            Máy rửa bát </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/noi-chao-pc293.html" title="Nồi chảo"
                                                           class="name">
                                                            Nồi chảo </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phu-kien-phong-bep-pc292.html"
                                                           title="Phụ kiện nhà bếp" class="name">
                                                            Phụ kiện nhà bếp </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/chau-rua-bat-pc232.html"
                                                           title="Chậu rửa bát" class="name">
                                                            Chậu rửa bát </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-loc-nuoc-pc251.html"
                                                           title="Máy lọc nước" class="name">
                                                            Máy lọc nước </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/may-say-pc315.html" title="Máy sấy bát"
                                                           class="name">
                                                            Máy sấy bát </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                </div>
                                                <div class="col-number col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <li class="level_0 li-product-menu-item closed" id="li-menu_item_1112">
                                        <a href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-pc214.html"
                                           id="menu_item_1112" class="menu_item_a" title="Hóa chất phụ gia xây dựng">
                                            <span class="text-menu">Hóa chất phụ gia xây dựng</span>
                                            <svg fill="gray" width="10px" height="10px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                <path d="M17.525 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L205.947
                                                256 10.454 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97
                                                0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L34.495 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class="" fill="#808080"></path>
                                            </svg>
                                        </a>
                                        <!--	LEVEL 1			-->
                                        <div class="level1">
                                            <div class="subcat cls scroll_bar">
                                                <div class="col-number col-1">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/thuong-hieu.html"
                                                           title="Thương hiệu sản phẩm" class="name">
                                                            Thương hiệu sản phẩm </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-asia-star-pcm214.html"
                                                               title="ASIA-STAR">ASIA-STAR</a>
                                                            <a href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-webber-pcm214.html"
                                                               title="WEBBER">WEBBER</a>
                                                            <a href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-miracle-water-pcm214.html"
                                                               title="MIRACLE WATER">MIRACLE WATER</a>
                                                            <a href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-sika-pcm214.html"
                                                               title="SIKA">SIKA</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-number col-2">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/keo-dan-gach-pc276.html"
                                                           title="Keo dán gạch" class="name">
                                                            Keo dán gạch </a>
                                                        <div class="manu mn_lv2">
                                                            <a href="https://hailinh.vn/keo-dan-gach-trong-nha-pc215.html"
                                                               title="Keo dán gạch trong nhà">Keo dán gạch trong nhà</a>
                                                            <a href="https://hailinh.vn/keo-dan-gach-ngoai-troi-pc216.html"
                                                               title="Keo dán gạch ngoài trời">Keo dán gạch ngoài trời</a>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/keo-cha-ron-pc217.html"
                                                           title="Keo chà ron" class="name">
                                                            Keo chà ron </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/chong-tham-dan-dung-pc218.html"
                                                           title="Chống thấm dân dụng" class="name">
                                                            Chống thấm dân dụng </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-3">
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/tay-rua-xu-ly-vet-ban-pc219.html"
                                                           title="Tẩy rửa sử lý vết bẩn" class="name">
                                                            Tẩy rửa sử lý vết bẩn </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/phu-gia-chat-ket-dinh-pc277.html"
                                                           title="Phụ gia , chất kết dính" class="name">
                                                            Phụ gia , chất kết dính </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/danh-bong-pc220.html" title="Đánh bóng"
                                                           class="name">
                                                            Đánh bóng </a>
                                                    </div>
                                                    <div class="col">
                                                        <a href="https://hailinh.vn/vua-chuyen-dung-pc280.html"
                                                           title="Vữa chuyên dụng" class="name">
                                                            Vữa chuyên dụng </a>
                                                    </div>
                                                </div>
                                                <div class="col-number col-4">
                                                </div>
                                                <div class="col-number col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <!--	END LEVEL 1			-->
                                    </li>
                                    <!--	CHILDREN				-->
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="top_menu">
                    <div class="dcjq-mega-menu">
                        <div class="sb-toggle-left navbar-left">
                            <svg x="0px" y="0px" viewBox="0 0 73.168 73.168" style="enable-background:new 0 0 73.168 73.168;" xml:space="preserve">
                                <g>
                                    <g id="Navigation">
                                        <g>
                                            <path d="M4.242,14.425h64.684c2.344,0,4.242-1.933,4.242-4.324c0-2.385-1.898-4.325-4.242-4.325H4.242
                                            C1.898,5.776,0,7.716,0,10.101C0,12.493,1.898,14.425,4.242,14.425z M68.926,32.259H4.242C1.898,32.259,0,34.2,0,36.584
                                            c0,2.393,1.898,4.325,4.242,4.325h64.684c2.344,0,4.242-1.933,4.242-4.325C73.168,34.2,71.27,32.259,68.926,32.259z
                                            M68.926,58.742H4.242C1.898,58.742,0,60.683,0,63.067c0,2.393,1.898,4.325,4.242,4.325h64.684c2.344,0,4.242-1.935,4.242-4.325
                                            C73.168,60.683,71.27,58.742,68.926,58.742z"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <ul id="megamenu" class="menu mainmenu2 mypopup cls">
                            <li class="level_0 sort">
                                <span id="dot"><span class="ping"></span></span>
                                <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 497.882 497.882" height="512" viewBox="0 0 497.882 497.882" width="512">
                                    <path d="m295.617 0c-106.104 61.135-93.353 233.382-93.353 233.382s-46.676-15.559-46.676-85.573c-55.688
                                     32.291-93.353 94.357-93.353 163.367 0 103.115 83.591 186.706 186.706 186.706s186.706-83.591
                                     186.706-186.706c-.001-151.698-140.03-182.816-140.03-311.176zm-30.276
                                     433.549c-37.518 9.354-75.517-13.477-84.873-50.997-9.354-37.518 13.477-75.519 50.997-84.873
                                     90.58-22.584 101.932-73.521 101.932-73.521s45.169 181.16-68.056 209.391z"fill="#FC8600">
                                    </path>
                                </svg>
                                <a href="https://hailinh.vn/khuyen-mai.html" id="menu_item_1006" class="menu_item_a" title="Khuyến mại hot">
                                    Khuyến mại hot </a>
                                <!--	LEVEL 1			-->
                                <!--	end LEVEL 1			-->
                            </li>
                            <li class="level_0 sort">
                                <svg x="0px" y="0px" viewBox="0 0 25.999 25.999" style="enable-background:new 0 0 25.999 25.999;" xml:space="preserve">
                                    <g fill="#FC8600">
                                        <path d="M25.856,8.485l-3-5c-0.003-0.005-0.009-0.008-0.013-0.013c-0.015-0.023-0.031-0.044-0.048-0.066
                                        c-0.029-0.038-0.062-0.073-0.096-0.107s-0.068-0.068-0.107-0.096c-0.016-0.012-0.029-0.027-0.045-0.038
                                        c-0.006-0.004-0.014-0.006-0.021-0.01c-0.02-0.012-0.043-0.019-0.063-0.03c-0.028-0.015-0.051-0.036-0.081-0.049
                                        c-0.015-0.006-0.031-0.006-0.046-0.011c-0.047-0.017-0.094-0.026-0.142-0.036c-0.014-0.003-0.028-0.008-0.042-0.011
                                        c-0.011-0.002-0.02-0.01-0.031-0.011c-0.003,0-0.007,0.001-0.01,0.001c-0.022-0.003-0.044-0.001-0.067-0.002
                                        C22.029,3.006,22.015,3,21.999,3H8.73C8.525,3,8.326,3.063,8.158,3.18l-5.731,4C2.404,7.196,2.39,7.219,2.369,7.236
                                        C2.346,7.255,2.32,7.267,2.299,7.288C2.283,7.304,2.274,7.326,2.258,7.343c-0.04,0.045-0.074,0.092-0.105,0.143
                                        C2.139,7.51,2.117,7.527,2.105,7.552l-2,4c-0.192,0.385-0.117,0.85,0.188,1.154C0.484,12.899,0.74,13,0.999,13
                                        c0.151,0,0.305-0.034,0.447-0.105l0.553-0.277V22c0,0.553,0.448,1,1,1h15c0.147,0,0.285-0.036,0.411-0.093
                                        c0.034-0.015,0.06-0.043,0.092-0.062c0.087-0.052,0.17-0.111,0.24-0.189c0.012-0.013,0.029-0.018,0.04-0.031l4-5
                                        c0.14-0.178,0.217-0.397,0.217-0.625v-4.586l2.707-2.707C26.03,9.383,26.093,8.879,25.856,8.485z M9.044,5h10.541l-2,2H6.179
                                        L9.044,5z M3.999,11V9h13v12h-13V11z M13.999,11c0,0.553-0.447,1-1,1h-5c-0.552,0-1-0.447-1-1c0-0.552,0.448-1,1-1h5
                                        C13.552,10,13.999,10.448,13.999,11z" fill="#FC8600"></path>
                                    </g>
                                </svg>
                                <a href="https://hailinh.vn/combo.html" id="menu_item_1007" class="menu_item_a" title="Combo sản phẩm">
                                    Combo sản phẩm </a>
                                <!--	LEVEL 1			-->
                                <span class="drop_down">
                                    <svg x="0px" y="0px" width="20" height="20px" viewBox="0 0 970.586 970.586"style="enable-background:new 0 0 970.586 970.586;" xml:space="preserve">
                                        <g fill="#FC8600">
                                            <path d="M44.177,220.307l363.9,296.4c22.101,18,48.9,27,75.8,27c26.901,0,53.701-9,75.801-27l366.699-298.7
                                            c51.4-41.9,59.101-117.4,17.2-168.8c-41.8-51.4-117.399-59.1-168.8-17.3l-290.901,237l-288.1-234.7c-51.4-41.8-127-34.1-168.8,17.3
                                            C-14.923,102.907-7.123,178.407,44.177,220.307z" fill="#FC8600">
                                            </path>
                                            <path d="M44.177,642.207l363.9,296.399c22.101,18,48.9,27,75.8,27c26.901,0,53.701-9,75.801-27l366.699-298.7
                                            c51.4-41.899,59.101-117.399,17.2-168.8c-41.899-51.399-117.399-59.1-168.8-17.2l-290.901,236.9l-288.1-234.6
                                            c-51.4-41.9-127-34.101-168.8,17.199C-14.923,524.807-7.123,600.406,44.177,642.207z" fill="#FC8600"></path>
                                        </g>
                                    </svg>
                                </span>
                                <div class="wrapper_children_level_0">
                                    <ul class="sb-submenu wrapper_children_level 0lindo_Combo sản phẩm">
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo337" title="Combo Toto + Belli">Combo
                                                Toto + Belli</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo338" title="Combo Cotto + Belli">Combo
                                                Cotto + Belli</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo332" title="Combo Viglacera">Combo
                                                Viglacera</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo333" title="Combo Inax">Combo
                                                Inax</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo339" title="Combo ToTo">Combo
                                                ToTo</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo334" title="Combo Caesar">Combo
                                                Caesar</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo335" title="Combo Cotto">Combo
                                                Cotto</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/combo.html#catcombo336"
                                               title="Combo American Standard">Combo American Standard</a>
                                        </li>
                                    </ul>
                                </div>
                                <!--	end LEVEL 1			-->
                            </li>

                            <li class="level_0 sort">
                                <span id="icon_hot">Hot</span>
                                <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 512.007 512.007" height="512" viewBox="0 0 512.007 512.007" width="512">
                                    <g fill="#FC8600">
                                        <path d="m253.997 225.923c-2.168 5.2-7.046 8.745-12.656 9.199l-5.259.41 3.999 3.428c4.277
                                        3.662 6.138 9.404 4.834 14.883l-1.216 5.098 4.482-2.739c4.805-2.93 10.84-2.93 15.645 0l4.482
                                        2.739-1.216-5.098c-1.304-5.479.557-11.221 4.834-14.883l3.999-3.428-5.259-.41c-5.61-.454-10.488-3.999-12.656-9.199l-2.007-4.849z" fill="#FC8600"></path>
                                        <path d="m151.004 142.478v63.289c0 81.724 39.067 157.266 105 204.053 65.933-46.787 105-122.329
                                        105-204.053v-63.289c-36.489-7.471-71.719-20.537-105-38.921-33.267 18.369-68.496 31.436-105 38.921zm118.857 33.772 12.334
                                        29.692 32.08 2.563c6.035.483 11.191 4.556 13.066 10.313 1.875 5.771.088 12.1-4.512 16.04l-24.434 20.933 7.471 31.274c1.406
                                        5.903-.864 12.056-5.771 15.615-4.824 3.504-11.34 3.887-16.641.674l-27.451-16.772-27.451 16.772c-5.156 3.149-11.719
                                        2.9-16.641-.674-4.907-3.56-7.178-9.712-5.771-15.615l7.471-31.274-24.434-20.933c-4.6-3.94-6.387-10.269-4.512-16.04 1.875-5.757
                                        7.031-9.829 13.066-10.313l32.08-2.563 12.334-29.692c4.659-11.191 23.058-11.191 27.716 0z" fill="#FC8600"></path>
                                        <path d="m436.004 60.007c-87.246 0-145.474-39.653-167.402-54.58l-4.277-2.9c-5.039-3.369-11.602-3.369-16.641 0l-4.307
                                        2.915c-23.979 16.333-80.142 54.565-167.373 54.565-8.291 0-15 6.709-15 15v130.76c0 129.858 72.144 246.592
                                        188.291 304.658 2.109 1.055 4.409 1.582 6.709 1.582s4.6-.527 6.709-1.582c116.147-58.066
                                        188.291-174.8 188.291-304.658v-130.76c0-8.291-6.709-15-15-15zm-45 145.76c0 95.215-47.402 182.974-126.812 234.756-2.49
                                        1.626-5.332 2.432-8.188 2.432s-5.698-.806-8.188-2.432c-79.409-51.782-126.812-139.541-126.812-234.756v-75.74c0-7.324
                                        5.288-13.564 12.495-14.795 40.195-6.797 78.867-20.889 114.961-41.88 4.658-2.725 10.43-2.725 15.088 0 36.123 21.006
                                        74.795 35.098 114.946 41.88 7.222 1.23 12.51 7.471 12.51 14.795z" fill="#FC8600"></path>
                                    </g>
                                </svg>
                                <a href="https://hailinh.vn/san-pham-ban-chay.html" id="menu_item_1008" class="menu_item_a" title="Sản phẩm bán chạy"> Sản phẩm bán chạy </a>
                                <!--	LEVEL 1			-->
                                <!--	end LEVEL 1			-->
                            </li>

                            <li class="level_0 sort">
                                <svg xmlns="http://www.w3.org/2000/svg" height="511pt" viewBox="0 -21 511.98744 511" width="511pt">
                                    <path d="m133.320312 373.828125c-34.152343
                                    0-64.53125-21.867187-75.5625-54.421875l-.746093-2.453125c-2.601563-8.621094-3.691407-15.871094-3.691407-23.125v-145.453125l-51.753906 172.757812c-6.65625
                                    25.410157 8.511719 51.753907 33.960938 58.773438l329.878906 88.34375c4.117188 1.066406 8.234375 1.578125 12.289062 1.578125 21.246094
                                    0 40.660157-14.101563 46.101563-34.882813l19.21875-61.117187zm0 0" fill="#FC8600"></path>
                                    <path d="m191.988281 149.828125c23.53125 0 42.664063-19.136719
                                    42.664063-42.667969s-19.132813-42.667968-42.664063-42.667968-42.667969 19.136718-42.667969 42.667968 19.136719
                                    42.667969 42.667969 42.667969zm0 0" fill="#FC8600"></path>
                                    <path d="m458.652344.492188h-320c-29.394532 0-53.332032 23.9375-53.332032 53.335937v234.664063c0 29.398437 23.9375
                                    53.335937 53.332032 53.335937h320c29.398437 0 53.335937-23.9375 53.335937-53.335937v-234.664063c0-29.398437-23.9375-53.335937-53.335937-53.335937zm-320
                                    42.667968h320c5.890625 0 10.667968 4.777344 10.667968
                                    10.667969v151.445313l-67.390624-78.636719c-7.148438-8.382813-17.496094-12.863281-28.609376-13.117188-11.050781.0625-21.417968
                                    4.96875-28.5 13.460938l-79.234374 95.101562-25.8125-25.75c-14.589844-14.589843-38.335938-14.589843-52.90625 0l-58.878907
                                    58.859375v-201.363281c0-5.890625 4.777344-10.667969 10.664063-10.667969zm0 0" fill="#FC8600"></path>
                                </svg>
                                <a href="https://hailinh.vn/bo-suu-tap.html" id="menu_item_1009" class="menu_item_a" title="Bộ sưu tập"> Bộ sưu tập </a>
                                <!--	LEVEL 1			-->
                                <!--	end LEVEL 1			-->
                            </li>
                            <li class="level_0 sort">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="45.854px" height="45.854px"
                                     viewBox="0 0 45.854 45.854" style="enable-background:new 0 0 45.854 45.854;" xml:space="preserve">
                                    <g fill="#FC8600">
                                        <path d="M31.751,26.057l-3.572-0.894c1.507-1.339,2.736-3.181,3.559-5.2c1.152-0.255,2.209-1.214,2.383-3.327
                                        c0.164-1.99-0.444-2.877-1.26-3.264c-0.01-0.237-0.021-0.469-0.035-0.696c1.049-5.812-3.103-7.843-3.426-7.941
                                        c-0.541-0.822-4.207-5.821-10.858-1.763c-0.974,0.594-2.519,1.764-3.344,2.942c-0.992,1.246-1.713,3.049-2.009,5.655
                                        c-0.205,0.022-0.409,0.054-0.611,0.1c-0.054-1.279,0.017-5.048,2.405-7.728c1.766-1.981,4.448-2.986,7.968-2.986
                                        c3.526,0,6.263,1.013,8.135,3.009c3.075,3.28,2.797,8.175,2.794,8.224c-0.018,0.262,0.182,0.489,0.443,0.506
                                        c0.012,0.001,0.021,0.001,0.033,0.001c0.248,0,0.457-0.193,0.475-0.445c0.015-0.217,0.312-5.345-3.045-8.933
                                        C29.727,1.116,26.754,0,22.95,0c-3.808,0-6.73,1.114-8.686,3.312c-2.859,3.213-2.706,7.66-2.625,8.695
                                        c-1.113,0.595-1.917,1.89-1.689,4.662c0.301,3.653,2.422,4.926,4.428,4.926c0.39,0,0.734-0.084,1.038-0.239
                                        c1.148,0.743,2.991,1.387,5.944,1.507c0.29,0.436,0.896,0.735,1.6,0.735c0.982,0,1.779-0.586,1.779-1.309
                                        c0-0.723-0.797-1.309-1.779-1.309c-0.801,0-1.478,0.389-1.701,0.924c-1.937-0.092-3.324-0.421-4.315-0.836
                                        c-1.353-1.819-1.996-4.907-1.996-6.671c0-0.653,0.021-1.26,0.059-1.825c6.256,0.21,9.975-2.319,12.007-4.516
                                        c2.617,2.608,3.548,6.138,3.858,7.825c-0.606,4.628-3.906,9.54-7.912,9.54c-1.723,0-3.316-0.909-4.62-2.313
                                        c-1.332-0.278-2.441-0.713-3.323-1.303c0.72,1.287,1.614,2.438,2.645,3.352l-3.58,0.898c-4.889,1.225-8.28,5.598-8.28,10.637v6.033
                                        c0,1.711,1.347,3.129,3.058,3.129h28.09c1.711,0,3.102-1.418,3.102-3.129v-6.033C40.053,31.654,36.64,27.279,31.751,26.057z
                                        M30.272,27.652l-4.65,3.183l-1.416-3.622c0.64-0.12,1.259-0.33,1.853-0.617L30.272,27.652z M19.786,26.589
                                        c0.583,0.28,1.194,0.488,1.827,0.61l-1.422,3.637l-4.651-3.184L19.786,26.589z M18.753,43.896h-5.352v-2.533
                                        c0-0.264-0.211-0.477-0.475-0.477c-0.263,0-0.475,0.213-0.475,0.477v2.533h-3.59c-0.66,0-1.159-0.512-1.159-1.172v-6.031
                                        c0-4.088,2.702-7.646,6.624-8.73l5.872,4.025c0.06,0.615,0.572,1.119,1.229,1.137L18.753,43.896z M22.149,28.463l0.446-1.145
                                        c0.121,0.006,0.542,0.008,0.631,0.004l0.445,1.141H22.149z M38.153,42.725c0,0.66-0.543,1.172-1.203,1.172h-3.605v-2.533
                                        c0-0.264-0.211-0.477-0.475-0.477s-0.476,0.213-0.476,0.477v2.533h-5.343l-2.674-10.771c0.652-0.017,1.162-0.515,1.229-1.123
                                        l5.9-4.039c3.922,1.083,6.645,4.642,6.645,8.729v6.031H38.153z" fill="#FC8600"></path>
                                    </g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                </svg>
                                <a href="https://hailinh.vn/tu-van-kn.html" id="menu_item_1010" class="menu_item_a" title="Tư vấn">Tư vấn </a>
                                <!--	LEVEL 1			-->
                                <!--	end LEVEL 1			-->
                            </li>
                            <li class="level_0 sort">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g fill="#FC8600">
                                        <g fill="#FC8600">
                                            <path d="M152.279,426.232l-3.753-0.938l3.753-0.938c16.299-4.074,27.682-18.654,27.682-35.454
                                                c0-20.152-16.394-36.547-36.546-36.547H87.631c-8.157,0-14.769,6.613-14.769,14.769s6.613,14.769,14.769,14.769h55.784
                                                c3.865,0,7.008,3.144,7.008,7.009c0,3.222-2.183,6.017-5.308,6.799l-37.037,9.259c-9.349,2.337-15.878,10.7-15.878,20.335
                                                c0,9.635,6.529,17.999,15.878,20.335l37.037,9.259c3.125,0.781,5.308,3.577,5.308,6.799c0,3.865-3.143,7.009-7.008,7.009H87.631
                                                c-8.157,0-14.769,6.613-14.769,14.769s6.613,14.769,14.769,14.769h55.784c20.152,0,36.546-16.395,36.546-36.547
                                                C179.961,444.888,168.577,430.307,152.279,426.232z" fill="#FC8600">
                                            </path>
                                        </g>
                                    </g>
                                    <g fill="#FC8600">
                                        <g fill="#FC8600">
                                            <path d="M265.695,410.525h-33.706v-14.316c0-7.895,6.423-14.316,14.316-14.316h48.476c8.157,0,14.769-6.613,14.769-14.769
                                                s-6.613-14.769-14.769-14.769h-48.476c-24.182,0-43.855,19.673-43.855,43.855v58.171c0,24.182,19.673,43.855,43.855,43.855h19.39
                                                c24.182,0,43.855-19.673,43.855-43.855C309.549,430.198,289.877,410.525,265.695,410.525z M265.695,468.697h-19.39
                                                c-7.895,0-14.316-6.422-14.316-14.316v-14.316h33.706c7.895,0,14.316,6.422,14.316,14.316S273.59,468.697,265.695,468.697z"fill="#FC8600">
                                            </path>
                                        </g>
                                    </g>
                                    <g fill="#FC8600">
                                        <g fill="#FC8600">
                                            <path d="M395.284,352.354h-19.39c-24.182,0-43.855,19.673-43.855,43.855v58.171c0,24.182,19.673,43.855,43.855,43.855h19.39
                                             c24.182,0,43.855-19.673,43.855-43.855v-58.171C439.138,372.027,419.466,352.354,395.284,352.354z M409.6,454.38
                                             c0,7.895-6.422,14.316-14.316,14.316h-19.39c-7.895,0-14.316-6.422-14.316-14.316v-58.171c0-7.895,6.422-14.316,14.316-14.316
                                             h19.39c7.895,0,14.316,6.422,14.316,14.316V454.38z" fill="#FC8600"></path>
                                        </g>
                                    </g>
                                    <g fill="#FC8600">
                                        <g fill="#FC8600">
                                            <polygon points="192,122.501 192,215.183 297.922,168.842" fill="#FC8600"></polygon>
                                        </g>
                                    </g>
                                    <g fill="#FC8600">
                                        <g fill="#FC8600">
                                            <path d="M438.154,13.765H73.846C33.127,13.765,0,46.892,0,87.611v162.462c0,40.719,33.127,73.846,73.846,73.846h364.308
                                            c40.719,0,73.846-33.127,73.846-73.846V87.611C512,46.892,478.873,13.765,438.154,13.765z M340.69,182.372l-157.538,68.923
                                            c-1.895,0.829-3.912,1.239-5.919,1.239c-2.831,0-5.644-0.813-8.085-2.408c-4.172-2.728-6.686-7.377-6.686-12.361V99.919
                                            c0-4.984,2.514-9.632,6.686-12.361c4.17-2.728,9.438-3.169,14.004-1.17l157.538,68.923c5.375,2.352,8.849,7.663,8.849,13.531
                                            S346.065,180.02,340.69,182.372z" fill="#FC8600"></path>
                                        </g>
                                    </g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                </svg>
                                <a href="https://hailinh.vn/video.html" id="menu_item_1011" class="menu_item_a" title="Video 360 độ"> Video 360 độ </a>
                                <!--	LEVEL 1			-->
                                <!--	end LEVEL 1			-->
                            </li>

                            <li class="level_0 sort">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.867 477.867" style="enable-background:new 0 0 477.867 477.867;" xml:space="preserve">
                                    <g fill="#FC8600">
                                        <g fill="#FC8600">
                                            <path d="M460.8,119.467h-85.333v-102.4C375.467,7.641,367.826,0,358.4,0H17.067C7.641,0,0,7.641,0,17.067V409.6
                                            c0,37.703,30.564,68.267,68.267,68.267H409.6c37.703,0,68.267-30.564,68.267-68.267V136.533
                                            C477.867,127.108,470.226,119.467,460.8,119.467z M136.533,85.333h102.4c9.426,0,17.067,7.641,17.067,17.067
                                            s-7.641,17.067-17.067,17.067h-102.4c-9.426,0-17.067-7.641-17.067-17.067S127.108,85.333,136.533,85.333z M290.133,409.6h-204.8
                                            c-9.426,0-17.067-7.641-17.067-17.067s7.641-17.067,17.067-17.067h204.8c9.426,0,17.067,7.641,17.067,17.067
                                            S299.559,409.6,290.133,409.6z M290.133,341.333h-204.8c-9.426,0-17.067-7.641-17.067-17.067c0-9.426,7.641-17.067,17.067-17.067
                                            h204.8c9.426,0,17.067,7.641,17.067,17.067C307.2,333.692,299.559,341.333,290.133,341.333z M290.133,273.067h-204.8
                                            c-9.426,0-17.067-7.641-17.067-17.067c0-9.426,7.641-17.067,17.067-17.067h204.8c9.426,0,17.067,7.641,17.067,17.067
                                            C307.2,265.426,299.559,273.067,290.133,273.067z M290.133,204.8h-204.8c-9.426,0-17.067-7.641-17.067-17.067
                                            c0-9.426,7.641-17.067,17.067-17.067h204.8c9.426,0,17.067,7.641,17.067,17.067C307.2,197.159,299.559,204.8,290.133,204.8z
                                            M443.733,409.6c0,18.851-15.282,34.133-34.133,34.133s-34.133-15.282-34.133-34.133v-256h68.267V409.6z" fill="#FC8600"></path>
                                        </g>
                                    </g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                    <g fill="#FC8600"></g>
                                </svg>
                                <a href="https://hailinh.vn/tin-tuc.html" id="menu_item_1012" class="menu_item_a" title="Tin tức"> Tin tức </a>
                                <!--	LEVEL 1			-->
                                <span class="drop_down">
                                    <svg x="0px" y="0px" width="20" height="20px" viewBox="0 0 970.586 970.586" style="enable-background:new 0 0 970.586 970.586;" xml:space="preserve">
                                        <g fill="#FC8600">
                                            <path d="M44.177,220.307l363.9,296.4c22.101,18,48.9,27,75.8,27c26.901,0,53.701-9,75.801-27l366.699-298.7
                                            c51.4-41.9,59.101-117.4,17.2-168.8c-41.8-51.4-117.399-59.1-168.8-17.3l-290.901,237l-288.1-234.7c-51.4-41.8-127-34.1-168.8,17.3
                                            C-14.923,102.907-7.123,178.407,44.177,220.307z" fill="#FC8600"></path>
                                            <path d="M44.177,642.207l363.9,296.399c22.101,18,48.9,27,75.8,27c26.901,0,53.701-9,75.801-27l366.699-298.7
                                            c51.4-41.899,59.101-117.399,17.2-168.8c-41.899-51.399-117.399-59.1-168.8-17.2l-290.901,236.9l-288.1-234.6
                                            c-51.4-41.9-127-34.101-168.8,17.199C-14.923,524.807-7.123,600.406,44.177,642.207z"
                                                  fill="#FC8600"></path>
                                        </g>
                                    </svg>
                                </span>
                                <div class="wrapper_children_level_0">
                                    <ul class="sb-submenu wrapper_children_level 0lindo_Tin tức">
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/tin-hai-linh-kn.html" title="Tin nội bộ">Tin nội
                                                bộ</a>
                                        </li>
                                        <li class="level_1">
                                            <a href="https://hailinh.vn/du-an-kn.html" title="Dự án">Dự án</a>
                                        </li>
                                    </ul>
                                </div>
                            </li> 
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</header>

