<?php
include 'header.php';
?>
<div class="clear"></div>
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs_wrapper" itemscope="" itemtype="http://schema.org/WebPage">
            <ul class="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope"
                    itemtype="http://schema.org/ListItem">
                    <a title="Hải Linh" href="https://hailinh.vn/" itemprop="item">
                        <span itemprop="name">Trang chủ</span>
                        <meta content="1" itemprop="position">
                    </a>
                </li>
                <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                    <a title="Tin tức" href="javascript:void(0)" itemprop="item">
                        <span itemprop="name">Video</span>
                        <meta content="2" itemprop="position">
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>
<div class="main_wrapper">
    <div class="container container_main_wrapper">
        <div class="main-area main-area-1col main-area-full">
            <div class="videos-home">
                <div class="page_title">
                    <span><h1>Video</h1></span>
                </div>
                <div class="clear"></div>
                <div class="default_menu_videos">
                    <div class="title-menu">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px" y="0px" viewBox="0 0 455 455" style="enable-background:new 0 0 455 455;"
                             xml:space="preserve">
                            <g>
                                <rect y="312.5" width="455" height="30"></rect>
                                <rect y="212.5" width="455" height="30"></rect>
                                <rect y="112.5" width="455" height="30"></rect>
                            </g>
                        </svg>
                        Danh mục
                    </div>
                    <div class="item">
                        <a href=" https://hailinh.vn/video-cong-ty-hai-linh-cv1044.html" title="Hải Linh">Hải Linh</a>
                    </div>
                    <div class="item">
                        <a href=" https://hailinh.vn/video-thiet-bi-ve-sinh-cv1040.html" title="Thiết bị vệ sinh">Thiết
                            bị vệ sinh</a>
                    </div>
                    <div class="item">
                        <a href=" https://hailinh.vn/video-gach-op-lat-cv1041.html" title="Gạch ốp lát">Gạch ốp lát</a>
                    </div>
                </div>
                <div class="video_one_block_body block_body cls">
                    <div class="video_item" id="one_video_play_area">
                        <div class="video_item_inner video_item_inner_has_img_custom">
                            <img class="videosssss " id="videoooooo" src="https://hailinh.vn/images/videos/large/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg" alt="" link-video="https://www.youtube.com/embed/17nz5cmBlgs">
                            <div class="play-icon">
                                <span class="play-video play-video-big">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                        <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                            </path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                            </path>
                                        </g>
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="video-name-pages">
                            <div class="video-name-inner">
                                <h1>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích nhất hiện nay</h1>
                            </div>
                        </div>
                    </div>
                    <div class="list_video_below cls">
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                        <div data-link="https://www.youtube.com/embed/PZpsIIDCxBE" data-title="" class="video_item_li cls" onclick="reload_video(this)">
                            <div class="image">
                                <img class="" src="https://hailinh.vn/images/videos/small/ve-cong-ty-hai-linh_1606095192.jpg" alt="">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512"
                                             height="512">
                                            <g id="_x31_-Video">
                                                <path style="fill:#DD352E;"
                                                      d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61
                                                      C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z">
                                                </path>
                                                <path style="fill:#FFFFFF;"
                                                      d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427
                                                      l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z">
                                                </path>
                                            </g>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <h3 class="title">Giới thiệu công ty THHH Kinh doanh và Thương mại Hải Linh</h3>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="videos-all">
                    <div class="videos-all-title">
                        Video mới nhất
                    </div>
                    <div class="videos-grid">
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_inner">
                                <div class="play-icon">
                                    <span class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 44" style="enable-background:new 0 0 58 44;" xml:space="preserve" width="512" height="512">
                                          <g id="_x31_-Video">
                                            <path style="fill:#DD352E;" d="M52.305,44H5.695C2.55,44,0,41.45,0,38.305V5.695C0,2.55,2.55,0,5.695,0h46.61   C55.45,0,58,2.55,58,5.695v32.61C58,41.45,55.45,44,52.305,44z"></path>
                                            <path style="fill:#FFFFFF;" d="M21,32.53V11.47c0-1.091,1.187-1.769,2.127-1.214l17.82,10.53c0.923,0.546,0.923,1.882,0,2.427   l-17.82,10.53C22.187,34.299,21,33.621,21,32.53z"></path>
                                          </g>
                                        </svg>
                                  </span>
                                </div>
                                <div>
                                    <a href="" title="y" rel="nofollow" class="item-img">
                                        <img src="https://hailinh.vn/images/videos/resized/gach-lat-san-vuon-viglacera-duoc-yeu-thich-nhat_1608195737.jpg " alt="" title="" style="">
                                    </a>
                                </div>
                                <a href="https://hailinh.vn/top-4-mau-gach-lat-san-vuon-dep-duoc-yeu-thich-nhat-hien-nay-vd535.html" title="" class="name"><h3>TOP 4 mẫu gạch lát sân vườn ĐẸP được yêu thích...</h3></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="pagination">
                            <span title="Page 1" class="current"><span>1</span></span>
                            <a class="other-page" title="Page 2" href="/video-page2.html"><span>2</span></a>
                            <a class="next-page" title="Next page" href="/video-page2.html">›</a>
                            <a class="last-page" title="Last page" href="/video-page2.html">››</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="pos4 container">
        <div class="block_strengths strengths-_strengths strengths_0 block" id="block_id_132">
            <p class="block_title"><span>Hải Linh - Nơi hội tụ của các thương hiệu nổi tiếng</span></p>
            <div class="container">
                <div class="block-strengths block-strengths-2 block-strengths-row-2">
                    <div class="item">
                        <a href="#" alt="Cam kết bán hàng chính hãng" class="asvg">
                            <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                    <g>
                                        <path d="M493.563,431.87l-58.716-125.913c-32.421,47.207-83.042,80.822-141.639,91.015l49.152,105.401
                                            c6.284,13.487,25.732,12.587,30.793-1.341l25.193-69.204l5.192-2.421l69.205,25.193
                                            C486.63,459.696,499.839,445.304,493.563,431.87z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M256.001,0C154.815,0,72.485,82.325,72.485,183.516s82.331,183.516,183.516,183.516
                                            c101.186,0,183.516-82.325,183.516-183.516S357.188,0,256.001,0z M345.295,170.032l-32.541,31.722l7.69,44.804
                                            c2.351,13.679-12.062,23.956-24.211,17.585l-40.231-21.148l-40.231,21.147c-12.219,6.416-26.549-3.982-24.211-17.585l7.69-44.804
                                            l-32.541-31.722c-9.89-9.642-4.401-26.473,9.245-28.456l44.977-6.533l20.116-40.753c6.087-12.376,23.819-12.387,29.913,0
                                            l20.116,40.753l44.977,6.533C349.697,143.557,355.185,160.389,345.295,170.032z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M77.156,305.957L18.44,431.87c-6.305,13.497,7.023,27.81,20.821,22.727l69.204-25.193l5.192,2.421l25.193,69.205
                                            c5.051,13.899,24.496,14.857,30.793,1.342l49.152-105.401C160.198,386.779,109.578,353.165,77.156,305.957z">
                                        </path>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="#" alt="Cam kết bán hàng chính hãng">Cam kết bán hàng chính hãng</a>
                        </div>
                    </div>
                    <div class="item">
                        <a href="#" alt="14 năm kinh nghiệm" class="asvg">
                            <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                    <g>
                                        <path d="M122.763,306.519V156.766c0-11.732,3.094-22.747,8.488-32.3c-12.26,0-85.918,0-98.238,0v0.006
                                            c-17.906,0.124-32.385,14.674-32.385,32.609v149.752c0,10.118,8.202,18.32,18.32,18.32h23.826l6.646,136.613
                                            c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858l5.217-107.249
                                            C137.304,348.68,122.763,328.929,122.763,306.519z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <circle cx="412.182" cy="52.901" r="52.274"></circle>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M480.467,124.825l0.002-0.044c-8.09,0-81.852,0-99.539,0c5.284,9.48,8.307,20.384,8.307,31.987V306.52
                                            c0,22.085-14.07,41.323-34.134,48.572l5.205,106.989c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858
                                            l6.697-137.674h22.346c10.118,0,18.32-8.202,18.32-18.32V157.395C511.373,139.955,497.685,125.714,480.467,124.825z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M325.024,124.196l0.002-0.044h-51.693l10.126,10.126c2.379,2.379,2.379,6.238,0,8.617l-10.678,10.678l8.313,93.003
                                            c0.288,3.217-0.578,6.431-2.445,9.067l-17.674,24.973c-1.143,1.614-2.997,2.574-4.973,2.574c-1.976,0-3.832-0.959-4.973-2.574
                                            l-17.674-24.973c-1.865-2.636-2.732-5.85-2.445-9.067l8.313-93.003l-10.678-10.678c-2.379-2.379-2.379-6.238,0-8.617
                                            l10.126-10.126h-50.213v0.006c-17.906,0.124-32.384,14.673-32.384,32.609v149.752c0,10.118,8.202,18.32,18.32,18.32h23.826
                                            l1.62,33.309l5.025,103.304c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858l5.077-104.366
                                            l1.62-33.309h22.346c10.118,0,18.32-8.202,18.32-18.32V156.766h0.001C355.929,139.327,342.241,125.086,325.024,124.196z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path
                                                d="M256.741,0c-28.869,0-52.272,23.403-52.272,52.272c0,28.863,23.41,52.274,52.272,52.274    c28.866,0,52.272-23.414,52.272-52.272C309.013,23.403,285.61,0,256.741,0z"></path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <circle cx="101.294" cy="52.584" r="52.274"></circle>
                                    </g>
                                </g> </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="https://www.youtube.com/watch?v=Q9NMzGroYlk&amp;feature=emb_logo"
                               alt="14 năm kinh nghiệm">14 năm kinh nghiệm</a>

                        </div>
                    </div>
                    <div class="item">
                        <a href="#" alt="Đối tác của các thương hiệu hàng đầu" class="asvg">
                            <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512">
                                <g>
                                    <path d="m386.158 8.733c-5.858-5.858-15.355-5.858-21.213 0l-36.478 36.478c-19.923-10.769-45.386-7.748-62.199
                                         9.065l-69.416 69.416c-14.041 14.041-14.041 36.806 0 50.846 14.041 14.041 36.806 14.041 50.846 0l25.09-25.09c38.912
                                         32.448 92.273 42.6 140.591 26.252l66.82 66.82c8.376-18.192 5.834-40.216-7.65-56.069l35.058-35.058c5.858-5.858 5.858-15.355
                                         0-21.213-.101-.101-.211-.187-.315-.284l.015-.015z">
                                    </path>
                                    <path d="m276.459 400.011c-14.041-14.041-36.806-14.041-50.847 0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.847
                                        0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.846 0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.847
                                        0l-46.272 46.272c-14.041 14.041-14.041 36.806 0 50.847 14.041 14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.846 14.041
                                        14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.847 14.041 14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.846
                                        14.041 14.041 36.806 14.041 50.846 0l46.272-46.272c14.044-14.043 14.044-36.807.003-50.848z">
                                    </path>
                                    <path d="m276.16 188.504-7.248 7.248c-25.715 25.715-67.558 25.715-93.273 0s-25.715-67.558 0-93.273l57.514-57.514c-16.157-6.188-34.547-4.891-49.768
                                        3.892l-36.329-36.329c-5.858-5.858-15.355-5.858-21.213 0l-121.45 121.449c-5.858 5.858-5.858 15.355 0 21.213l36.329 36.329c-8.783 15.221-10.08 33.611-3.892
                                        49.768l15.029-15.029c25.715-25.715 67.558-25.715 93.273 0 9.374 9.374 15.315 20.895 17.854 32.992 12.098 2.54 23.618 8.48 32.992 17.854s15.314
                                        20.895 17.854 32.993c12.098 2.54 23.618 8.48 32.992 17.853 9.374 9.374 15.315 20.895 17.854 32.992 12.098 2.54 23.618 8.48 32.992 17.854 25.715
                                        25.715 25.715 67.558 0 93.273l-18.562 18.562 6.497 6.497c14.041 14.041 36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041
                                        36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041 36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041
                                        36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847l-84.475-84.475c-43.996 9.707-89.992 2.097-128.358-20.761z">
                                    </path>
                                </g>
                            </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="https://hailinh.vn/thuong-hieu.html"
                               alt="Đối tác của các thương hiệu hàng đầu">Đối tác của các thương hiệu hàng đầu
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include 'footer.php';
?>