<?php
include 'header.php';
?>
<div class="clear"></div>
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs_wrapper" itemscope="" itemtype="http://schema.org/WebPage">
            <ul class="breadcrumb" itemscope="itemscope" itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope"
                    itemtype="http://schema.org/ListItem">
                    <a title="Hải Linh" href="https://hailinh.vn/" itemprop="item">
                        <span itemprop="name">Trang chủ</span>
                        <meta content="1" itemprop="position">
                    </a>
                </li>
                <li class="breadcrumb__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
                    <a title="Tin tức" href="javascript:void(0)" itemprop="item">
                        <span itemprop="name">Tin tức</span>
                        <meta content="2" itemprop="position">
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>
<div class="main_wrapper">
    <div class="container container_main_wrapper">
        <div class="main-area main-area-1col main-area-full">
            <div class="news_home news_page">
                <div class="img-title-cat page_title">
                    <span><h1>Tin tức</h1></span>
                    <div class="block_search_news search_news_0 blockssearch-news blocks0 block" id="block_id_136">
                        <div class="block_title">
                            <span>Tìm kiếm tin tức</span>
                        </div>
                        <div id="block_content" class="block_content">
                            <form action="https://hailinh.vn/tim-kiem-tin-tuc" name="search_form" id="search_form" method="get" onsubmit="javascript: submit_form_search_news();return false;">
                                <div class="search_form">
                                    <input type="text" placeholder="Nhập nội dung muốn tìm kiếm" name="keyword_news" class="keyword_news" value="">
                                    <button type="submit" class="button-search button">
                                        <svg aria-hidden="true" data-prefix="far" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-search fa-w-16">
                                            <path d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0
                                             93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3
                                              0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" class="">
                                            </path>
                                        </svg>
                                    </button>
                                    <input type="hidden" name="module" value="news">
                                    <input type="hidden" name="module" id="link_search_news" value="https://hailinh.vn/tim-kiem-tin-tuc">
                                    <input type="hidden" name="view" value="search">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="bg_white">
                    <div class="news_header cls">
                        <div class="header_l item">
                            <figure class="frame_img">

                                <a class="item-img"
                                   href="https://hailinh.vn/tu-van/bon-cau-gia-re-duoi-500-mua-hang-nao-va-mua-o-dau-n261.html">
                                    <img class=""
                                         src="https://hailinh.vn/images/news/2021/04/03/large/bon-cau-gia-re_1617414272.jpg"
                                         alt="Bồn cầu giá rẻ dưới 500 mua hãng nào và mua ở đâu?">
                                </a>
                            </figure>
                            <h3 class="title"><a
                                        href="https://hailinh.vn/tu-van/bon-cau-gia-re-duoi-500-mua-hang-nao-va-mua-o-dau-n261.html"
                                        title="Bồn cầu giá rẻ dưới 500 mua hãng nào và mua ở đâu?">Bồn cầu giá rẻ dưới
                                    500 mua hãng nào và mua ở đâu?</a></h3>
                            <div class="datetime">
                                03/04/2021
                            </div>

                            <div class="summary">Có rất nhiều thương hiệu bồn cầu trên thị trường từ sản xuất trong nước
                                tới nhập khẩu. Mỗi hãng sẽ có những mức giá khác nhau. Vậy bồn cầu giá rẻ dưới 500K có
                                nên mua, mua hàng nào và mua ở đâu để đảm bảo hàng chất lượng. Theo dõi ngay bài viết
                                dưới đây.
                            </div>


                        </div>
                        <div class="header_r">
                            <div class="header_r_inner">
                                <div class="item ">
                                    <div class="inner-item">
                                        <figure class="frame_img">

                                            <a class="item-img"
                                               href="https://hailinh.vn/tu-van/nhung-mau-gach-op-tuong-tay-ban-nha-30x90-duoc-ua-thich-tai-hai-linh-n259.html">
                                                <img class=""
                                                     src="https://hailinh.vn/images/news/2021/04/02/resized/gach-op-tuong-tay-ban-nha-duoc-ua-thich-tai-hai-linh_1617327173.jpg"
                                                     alt="Những mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh"
                                                     title="" style="">
                                            </a>
                                        </figure>
                                        <div class="frame_title">
                                            <h3 class="item_title"><a
                                                        href="https://hailinh.vn/tu-van/nhung-mau-gach-op-tuong-tay-ban-nha-30x90-duoc-ua-thich-tai-hai-linh-n259.html"
                                                        title="Những mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh">Những
                                                    mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="item ">
                                    <div class="inner-item">
                                        <figure class="frame_img">

                                            <a class="item-img"
                                               href="https://hailinh.vn/tu-van/tip-chon-gach-lat-nen-cho-nguoi-yeu-thien-nhien-n258.html">
                                                <img class=""
                                                     src="https://hailinh.vn/images/news/2021/04/01/resized/chon-gach-lat-nen-cho-nguoi-yeu-thien-nhien_1617241934.jpg"
                                                     alt="Tip chọn gạch lát nền cho người yêu thiên nhiên" title=""
                                                     style="">
                                            </a>
                                        </figure>
                                        <div class="frame_title">
                                            <h3 class="item_title"><a
                                                        href="https://hailinh.vn/tu-van/tip-chon-gach-lat-nen-cho-nguoi-yeu-thien-nhien-n258.html"
                                                        title="Tip chọn gạch lát nền cho người yêu thiên nhiên">Tip chọn
                                                    gạch lát nền cho người yêu thiên nhiên</a></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="item ">
                                    <div class="inner-item">
                                        <figure class="frame_img">

                                            <a class="item-img"
                                               href="https://hailinh.vn/tu-van/top-6-mau-gach-lat-nen-viglacera-40x40-cho-san-vuon-n257.html">
                                                <img class=""
                                                     src="https://hailinh.vn/images/news/2021/04/01/resized/gach-lat-nen-viglacera-san-vuon_1617240879.jpg"
                                                     alt="Top 6 mẫu gạch lát nền Viglacera 40x40 cho sân vườn" title=""
                                                     style="">
                                            </a>
                                        </figure>
                                        <div class="frame_title">
                                            <h3 class="item_title"><a
                                                        href="https://hailinh.vn/tu-van/top-6-mau-gach-lat-nen-viglacera-40x40-cho-san-vuon-n257.html"
                                                        title="Top 6 mẫu gạch lát nền Viglacera 40x40 cho sân vườn">Top
                                                    6 mẫu gạch lát nền Viglacera 40x40 cho sân vườn</a></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="item ">
                                    <div class="inner-item">
                                        <figure class="frame_img">

                                            <a class="item-img"
                                               href="https://hailinh.vn/tu-van/tieu-chi-lua-chon-dai-ly-cung-cap-gach-op-lat-taicera-uy-tin-nhat-hien-nay-n254.html">
                                                <img class=""
                                                     src="https://hailinh.vn/images/news/2021/03/26/resized/tieu-chi-lua-chon-dai-ly-gach-taicera_1616721883.jpg"
                                                     alt="Tiêu chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện nay"
                                                     title="" style="">
                                            </a>
                                        </figure>
                                        <div class="frame_title">
                                            <h3 class="item_title"><a
                                                        href="https://hailinh.vn/tu-van/tieu-chi-lua-chon-dai-ly-cung-cap-gach-op-lat-taicera-uy-tin-nhat-hien-nay-n254.html"
                                                        title="Tiêu chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện nay">Tiêu
                                                    chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện
                                                    nay</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- .header_r_inner -->
                        </div> <!-- .header_r -->
                    </div>

                    <div class="news_bottom cls">
                        <div class="news_bottom_l">
                            <div class="list-news">
                                <div class="item  cls">
                                    <figure class="frame_img">
                                        <a class="item-img"
                                           href="https://hailinh.vn/tin-hai-linh/hai-linh-chuc-mung-sinh-nhat-cbnv-quy-1-va-quoc-te-phu-nu-8-3-nam-2021-n253.html">
                                            <img class="lazy after-lazy"
                                                 alt="Hải Linh chúc mừng sinh nhật CBNV quý 1 và quốc tế phụ nữ 8/3 năm 2021"
                                                 src="https://hailinh.vn/images/news/2021/03/25/resized/sn-cty_1616637929.jpg"
                                                 style="opacity: 1; display: none;">
                                        </a>
                                    </figure>
                                    <div class="frame_right">
                                        <h3 class="title"><a
                                                    href="https://hailinh.vn/tin-hai-linh/hai-linh-chuc-mung-sinh-nhat-cbnv-quy-1-va-quoc-te-phu-nu-8-3-nam-2021-n253.html"
                                                    title="Hải Linh chúc mừng sinh nhật CBNV quý 1 và quốc tế phụ nữ 8/3 năm 2021">Hải
                                                Linh chúc mừng sinh nhật CBNV quý 1 và quốc tế phụ nữ 8/3 năm 2021</a>
                                        </h3>
                                        <div class="datetime">
                                            25/03/2021
                                        </div>
                                        <div class="sum">
                                            Với mong muốn tăng tinh đại đoàn kết cũng như tôn vinh những bông hoa đẹp
                                            nhất của công ty. Tại Hải Linh lại rộn ràng tổ chức sinh nhật cho các nhân
                                            viên có ngày sinh trong quý 1 năm 2021 đồng thời cũng tổ chức 8/3 cho tập
                                            thể các chị em xinh đẹp của công ty.
                                        </div>
                                    </div>
                                </div><!-- end.item  -->
                                <div class="item  cls">
                                    <figure class="frame_img">
                                        <a class="item-img"
                                           href="https://hailinh.vn/tu-van/dia-chi-uy-tin-mua-gach-tay-ban-nha-chinh-hang-tai-ha-noi-n252.html">
                                            <img class="lazy after-lazy"
                                                 alt="Địa chỉ uy tín mua gạch Tây Ban Nha chính hãng tại Hà Nội"
                                                 src="https://hailinh.vn/images/news/2021/03/25/resized/dia-chi-mua-gach-tbn_1616636362.jpg"
                                                 style="opacity: 1; display: none;">
                                        </a>
                                    </figure>
                                    <div class="frame_right">
                                        <h3 class="title"><a
                                                    href="https://hailinh.vn/tu-van/dia-chi-uy-tin-mua-gach-tay-ban-nha-chinh-hang-tai-ha-noi-n252.html"
                                                    title="Địa chỉ uy tín mua gạch Tây Ban Nha chính hãng tại Hà Nội">Địa
                                                chỉ uy tín mua gạch Tây Ban Nha chính hãng tại Hà Nội</a></h3>
                                        <div class="datetime">
                                            25/03/2021
                                        </div>
                                        <div class="sum">
                                            Khách hàng tại Hà Nội đang có nhu cầu tìm kiếm địa chỉ cung cấp, phân phối
                                            các sản phẩm gạch Tây Ban Nha chính hãng, chất lượng cao, giá tốt? Showroom
                                            Hải Linh là đại lý cung cấp các sản phẩm gạch ốp lát cao cấp, giao tận chân
                                            công trình.
                                        </div>
                                    </div>
                                </div><!-- end.item  -->
                                <div class="item  cls">
                                    <figure class="frame_img">
                                        <a class="item-img"
                                           href="https://hailinh.vn/tu-van/gach-op-lat-tay-ban-nha-gia-cuc-tot-tai-showroom-hai-linh-n251.html">
                                            <img class="lazy after-lazy"
                                                 alt="Gạch ốp lát Tây Ban Nha giá cực tốt tại Showroom Hải Linh"
                                                 src="https://hailinh.vn/images/news/2021/03/25/resized/gach-tbn-gia-tot-tai-hl_1616635638.jpg"
                                                 style="opacity: 1; display: none;">
                                        </a>
                                    </figure>
                                    <div class="frame_right">
                                        <h3 class="title"><a
                                                    href="https://hailinh.vn/tu-van/gach-op-lat-tay-ban-nha-gia-cuc-tot-tai-showroom-hai-linh-n251.html"
                                                    title="Gạch ốp lát Tây Ban Nha giá cực tốt tại Showroom Hải Linh">Gạch
                                                ốp lát Tây Ban Nha giá cực tốt tại Showroom Hải Linh</a></h3>
                                        <div class="datetime">
                                            25/03/2021
                                        </div>
                                        <div class="sum">
                                            Gạch Tây Ban Nha là dòng sản phẩm đang rất được ưa chuộng trên thị trường
                                            hiện nay. Xu hướng sử dụng các sản phẩm cao cấp, bắt kịp xu hướng mới đang
                                            rất được quan tâm. Nắm bắt được nhu cầu chung của xã hội, nhiều thương hiệu
                                            gạch ốp lát đã nghiên cứu thành công, cho ra đời các sản phẩm gạch Tây Ban
                                            Nha chất lượng, hứa hẹn mang tới một làn gió mới cho không gian sống thân
                                            yêu.
                                        </div>
                                    </div>
                                </div><!-- end.item  -->
                                <div class="item  cls">
                                    <figure class="frame_img">
                                        <a class="item-img"
                                           href="https://hailinh.vn/tu-van/review-mau-bon-cau-inax-ac-1008vrn-hot-nhat-tai-hai-linh-n250.html">
                                            <img class="lazy after-lazy"
                                                 data-src="https://hailinh.vn/images/news/2021/03/24/resized/review-bon-cau-inax-ac-1008vrn_1616557333.jpg"
                                                 alt="Review mẫu bồn cầu Inax AC-1008VRN Hot nhất tại Hải Linh"
                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                 style="opacity: 1;">
                                        </a>
                                    </figure>
                                    <div class="frame_right">
                                        <h3 class="title"><a
                                                    href="https://hailinh.vn/tu-van/review-mau-bon-cau-inax-ac-1008vrn-hot-nhat-tai-hai-linh-n250.html"
                                                    title="Review mẫu bồn cầu Inax AC-1008VRN Hot nhất tại Hải Linh">Review
                                                mẫu bồn cầu Inax AC-1008VRN Hot nhất tại Hải Linh</a></h3>
                                        <div class="datetime">
                                            24/03/2021
                                        </div>
                                        <div class="sum">
                                            AC-1008VRN là mã bồn cầu chiếm trọn niềm tin của người tiêu dùng và được mua
                                            nhiều nhất tại Showroom Hải Linh. Cùng review chi tiết mẫu bồn cầu Inax
                                            AC-1008VRN này trong bài viết dưới đây.
                                        </div>
                                    </div>
                                </div><!-- end.item  -->
                            </div>
                            <div class="clear"></div>
                            <div class="pagination"><span title="Page 1" class="current"><span>1</span></span><a
                                        class="other-page" title="Page 2"
                                        href="/tin-tuc-page2.html"><span>2</span></a><a class="other-page"
                                                                                        title="Page 3"
                                                                                        href="/tin-tuc-page3.html"><span>3</span></a><a
                                        class="other-page" title="Page 4"
                                        href="/tin-tuc-page4.html"><span>4</span></a><b>..</b><a class="next-page"
                                                                                                 title="Next page"
                                                                                                 href="/tin-tuc-page2.html">›</a><a
                                        class="last-page" title="Last page" href="/tin-tuc-page26.html">››</a></div>
                        </div>
                        <div class="news_bottom_r right_b">

                            <div class="block_newslist newslist_0 blocks_news_list blocks0 block" id="block_id_137">
                                <div class="block_title"><span>Bài viết gần đây</span></div>
                                <div class="news_list_body_default">
                                    <div class="news-item cls">

                                        <figure>
                                            <a href="https://hailinh.vn/tu-van/bon-cau-gia-re-duoi-500-mua-hang-nao-va-mua-o-dau-n261.html"
                                               title="Bồn cầu giá rẻ dưới 500 mua hãng nào và mua ở đâu?">
                                                <img src="https://hailinh.vn/images/news/2021/04/03/resized/bon-cau-gia-re_1617414272.jpg"
                                                     alt="Bồn cầu giá rẻ dưới 500 mua hãng nào và mua ở đâu?">
                                            </a>
                                        </figure>

                                        <div class="content-r">
                                            <a href="https://hailinh.vn/tu-van/bon-cau-gia-re-duoi-500-mua-hang-nao-va-mua-o-dau-n261.html"
                                               title="Bồn cầu giá rẻ dưới 500 mua hãng nào và mua ở đâu?">Bồn cầu giá rẻ
                                                dưới 500 mua hãng nào và mua ở đâu? </a>
                                            <div class="datetime">
                                                03/04/2021
                                            </div>
                                        </div>


                                        <div class="clear"></div>

                                    </div>
                                    <div class="news-item cls">

                                        <figure>
                                            <a href="https://hailinh.vn/tu-van/nhung-mau-gach-op-tuong-tay-ban-nha-30x90-duoc-ua-thich-tai-hai-linh-n259.html"
                                               title="Những mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh">
                                                <img src="https://hailinh.vn/images/news/2021/04/02/resized/gach-op-tuong-tay-ban-nha-duoc-ua-thich-tai-hai-linh_1617327173.jpg"
                                                     alt="Những mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh">
                                            </a>
                                        </figure>

                                        <div class="content-r">
                                            <a href="https://hailinh.vn/tu-van/nhung-mau-gach-op-tuong-tay-ban-nha-30x90-duoc-ua-thich-tai-hai-linh-n259.html"
                                               title="Những mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh">Những
                                                mẫu gạch ốp tường Tây Ban Nha 30x90 được ưa thích tại Hải Linh </a>
                                            <div class="datetime">
                                                02/04/2021
                                            </div>
                                        </div>


                                        <div class="clear"></div>

                                    </div>
                                    <div class="news-item cls">

                                        <figure>
                                            <a href="https://hailinh.vn/tu-van/tip-chon-gach-lat-nen-cho-nguoi-yeu-thien-nhien-n258.html"
                                               title="Tip chọn gạch lát nền cho người yêu thiên nhiên">
                                                <img src="https://hailinh.vn/images/news/2021/04/01/resized/chon-gach-lat-nen-cho-nguoi-yeu-thien-nhien_1617241934.jpg"
                                                     alt="Tip chọn gạch lát nền cho người yêu thiên nhiên">
                                            </a>
                                        </figure>

                                        <div class="content-r">
                                            <a href="https://hailinh.vn/tu-van/tip-chon-gach-lat-nen-cho-nguoi-yeu-thien-nhien-n258.html"
                                               title="Tip chọn gạch lát nền cho người yêu thiên nhiên">Tip chọn gạch lát
                                                nền cho người yêu thiên nhiên </a>
                                            <div class="datetime">
                                                01/04/2021
                                            </div>
                                        </div>


                                        <div class="clear"></div>

                                    </div>
                                    <div class="news-item cls">

                                        <figure>
                                            <a href="https://hailinh.vn/tu-van/top-6-mau-gach-lat-nen-viglacera-40x40-cho-san-vuon-n257.html"
                                               title="Top 6 mẫu gạch lát nền Viglacera 40x40 cho sân vườn">
                                                <img src="https://hailinh.vn/images/news/2021/04/01/resized/gach-lat-nen-viglacera-san-vuon_1617240879.jpg"
                                                     alt="Top 6 mẫu gạch lát nền Viglacera 40x40 cho sân vườn">
                                            </a>
                                        </figure>

                                        <div class="content-r">
                                            <a href="https://hailinh.vn/tu-van/top-6-mau-gach-lat-nen-viglacera-40x40-cho-san-vuon-n257.html"
                                               title="Top 6 mẫu gạch lát nền Viglacera 40x40 cho sân vườn">Top 6 mẫu
                                                gạch lát nền Viglacera 40x40 cho sân vườn </a>
                                            <div class="datetime">
                                                01/04/2021
                                            </div>
                                        </div>


                                        <div class="clear"></div>

                                    </div>
                                    <div class="news-item cls">

                                        <figure>
                                            <a href="https://hailinh.vn/tu-van/tieu-chi-lua-chon-dai-ly-cung-cap-gach-op-lat-taicera-uy-tin-nhat-hien-nay-n254.html"
                                               title="Tiêu chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện nay">
                                                <img src="https://hailinh.vn/images/news/2021/03/26/resized/tieu-chi-lua-chon-dai-ly-gach-taicera_1616721883.jpg"
                                                     alt="Tiêu chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện nay">
                                            </a>
                                        </figure>

                                        <div class="content-r">
                                            <a href="https://hailinh.vn/tu-van/tieu-chi-lua-chon-dai-ly-cung-cap-gach-op-lat-taicera-uy-tin-nhat-hien-nay-n254.html"
                                               title="Tiêu chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện nay">Tiêu
                                                chí lựa chọn đại lý cung cấp gạch ốp lát Taicera uy tín nhất hiện
                                                nay </a>
                                            <div class="datetime">
                                                26/03/2021
                                            </div>
                                        </div>


                                        <div class="clear"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="pos4 container">
        <div class="block_strengths strengths-_strengths strengths_0 block" id="block_id_132">
            <p class="block_title"><span>Hải Linh - Nơi hội tụ của các thương hiệu nổi tiếng</span></p>
            <div class="container">
                <div class="block-strengths block-strengths-2 block-strengths-row-2">
                    <div class="item">
                        <a href="#" alt="Cam kết bán hàng chính hãng" class="asvg">
                            <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                    <g>
                                        <path d="M493.563,431.87l-58.716-125.913c-32.421,47.207-83.042,80.822-141.639,91.015l49.152,105.401
                                            c6.284,13.487,25.732,12.587,30.793-1.341l25.193-69.204l5.192-2.421l69.205,25.193
                                            C486.63,459.696,499.839,445.304,493.563,431.87z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M256.001,0C154.815,0,72.485,82.325,72.485,183.516s82.331,183.516,183.516,183.516
                                            c101.186,0,183.516-82.325,183.516-183.516S357.188,0,256.001,0z M345.295,170.032l-32.541,31.722l7.69,44.804
                                            c2.351,13.679-12.062,23.956-24.211,17.585l-40.231-21.148l-40.231,21.147c-12.219,6.416-26.549-3.982-24.211-17.585l7.69-44.804
                                            l-32.541-31.722c-9.89-9.642-4.401-26.473,9.245-28.456l44.977-6.533l20.116-40.753c6.087-12.376,23.819-12.387,29.913,0
                                            l20.116,40.753l44.977,6.533C349.697,143.557,355.185,160.389,345.295,170.032z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M77.156,305.957L18.44,431.87c-6.305,13.497,7.023,27.81,20.821,22.727l69.204-25.193l5.192,2.421l25.193,69.205
                                            c5.051,13.899,24.496,14.857,30.793,1.342l49.152-105.401C160.198,386.779,109.578,353.165,77.156,305.957z">
                                        </path>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="#" alt="Cam kết bán hàng chính hãng">Cam kết bán hàng chính hãng</a>
                        </div>
                    </div>
                    <div class="item">
                        <a href="#" alt="14 năm kinh nghiệm" class="asvg">
                            <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g>
                                    <g>
                                        <path d="M122.763,306.519V156.766c0-11.732,3.094-22.747,8.488-32.3c-12.26,0-85.918,0-98.238,0v0.006
                                            c-17.906,0.124-32.385,14.674-32.385,32.609v149.752c0,10.118,8.202,18.32,18.32,18.32h23.826l6.646,136.613
                                            c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858l5.217-107.249
                                            C137.304,348.68,122.763,328.929,122.763,306.519z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <circle cx="412.182" cy="52.901" r="52.274"></circle>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M480.467,124.825l0.002-0.044c-8.09,0-81.852,0-99.539,0c5.284,9.48,8.307,20.384,8.307,31.987V306.52
                                            c0,22.085-14.07,41.323-34.134,48.572l5.205,106.989c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858
                                            l6.697-137.674h22.346c10.118,0,18.32-8.202,18.32-18.32V157.395C511.373,139.955,497.685,125.714,480.467,124.825z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M325.024,124.196l0.002-0.044h-51.693l10.126,10.126c2.379,2.379,2.379,6.238,0,8.617l-10.678,10.678l8.313,93.003
                                            c0.288,3.217-0.578,6.431-2.445,9.067l-17.674,24.973c-1.143,1.614-2.997,2.574-4.973,2.574c-1.976,0-3.832-0.959-4.973-2.574
                                            l-17.674-24.973c-1.865-2.636-2.732-5.85-2.445-9.067l8.313-93.003l-10.678-10.678c-2.379-2.379-2.379-6.238,0-8.617
                                            l10.126-10.126h-50.213v0.006c-17.906,0.124-32.384,14.673-32.384,32.609v149.752c0,10.118,8.202,18.32,18.32,18.32h23.826
                                            l1.62,33.309l5.025,103.304c1.36,27.954,24.42,49.92,52.407,49.92c27.391,0,49.962-21.499,51.292-48.858l5.077-104.366
                                            l1.62-33.309h22.346c10.118,0,18.32-8.202,18.32-18.32V156.766h0.001C355.929,139.327,342.241,125.086,325.024,124.196z">
                                        </path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path
                                                d="M256.741,0c-28.869,0-52.272,23.403-52.272,52.272c0,28.863,23.41,52.274,52.272,52.274    c28.866,0,52.272-23.414,52.272-52.272C309.013,23.403,285.61,0,256.741,0z"></path>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <circle cx="101.294" cy="52.584" r="52.274"></circle>
                                    </g>
                                </g> </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="https://www.youtube.com/watch?v=Q9NMzGroYlk&amp;feature=emb_logo"
                               alt="14 năm kinh nghiệm">14 năm kinh nghiệm</a>

                        </div>
                    </div>
                    <div class="item">
                        <a href="#" alt="Đối tác của các thương hiệu hàng đầu" class="asvg">
                            <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512">
                                <g>
                                    <path d="m386.158 8.733c-5.858-5.858-15.355-5.858-21.213 0l-36.478 36.478c-19.923-10.769-45.386-7.748-62.199
                                         9.065l-69.416 69.416c-14.041 14.041-14.041 36.806 0 50.846 14.041 14.041 36.806 14.041 50.846 0l25.09-25.09c38.912
                                         32.448 92.273 42.6 140.591 26.252l66.82 66.82c8.376-18.192 5.834-40.216-7.65-56.069l35.058-35.058c5.858-5.858 5.858-15.355
                                         0-21.213-.101-.101-.211-.187-.315-.284l.015-.015z">
                                    </path>
                                    <path d="m276.459 400.011c-14.041-14.041-36.806-14.041-50.847 0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.847
                                        0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.846 0 14.041-14.041 14.041-36.806 0-50.846-14.041-14.041-36.806-14.041-50.847
                                        0l-46.272 46.272c-14.041 14.041-14.041 36.806 0 50.847 14.041 14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.846 14.041
                                        14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.847 14.041 14.041 36.806 14.041 50.846 0-14.041 14.041-14.041 36.806 0 50.846
                                        14.041 14.041 36.806 14.041 50.846 0l46.272-46.272c14.044-14.043 14.044-36.807.003-50.848z">
                                    </path>
                                    <path d="m276.16 188.504-7.248 7.248c-25.715 25.715-67.558 25.715-93.273 0s-25.715-67.558 0-93.273l57.514-57.514c-16.157-6.188-34.547-4.891-49.768
                                        3.892l-36.329-36.329c-5.858-5.858-15.355-5.858-21.213 0l-121.45 121.449c-5.858 5.858-5.858 15.355 0 21.213l36.329 36.329c-8.783 15.221-10.08 33.611-3.892
                                        49.768l15.029-15.029c25.715-25.715 67.558-25.715 93.273 0 9.374 9.374 15.315 20.895 17.854 32.992 12.098 2.54 23.618 8.48 32.992 17.854s15.314
                                        20.895 17.854 32.993c12.098 2.54 23.618 8.48 32.992 17.853 9.374 9.374 15.315 20.895 17.854 32.992 12.098 2.54 23.618 8.48 32.992 17.854 25.715
                                        25.715 25.715 67.558 0 93.273l-18.562 18.562 6.497 6.497c14.041 14.041 36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041
                                        36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041 36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847 14.041 14.041
                                        36.806 14.041 50.847 0 14.041-14.041 14.041-36.806 0-50.847l-84.475-84.475c-43.996 9.707-89.992 2.097-128.358-20.761z">
                                    </path>
                                </g>
                            </svg>
                        </a>
                        <div class="content-right">
                            <a class="title" href="https://hailinh.vn/thuong-hieu.html"
                               alt="Đối tác của các thương hiệu hàng đầu">Đối tác của các thương hiệu hàng đầu
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include 'footer.php';
?>