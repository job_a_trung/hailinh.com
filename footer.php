<div class="pos7 hello">
    <div class="container">
        <div class="block_address address-address-tab cls address_0 block" id="block_id_116">
            <p class="block_title"> <span>Công Ty TNHH Kinh Doanh Thương Mại Hải Linh</span></p>
            <div class="block_address_tab">
                <div class="tabcontent">
                    <div class="region_1 regions active">
                        <div class="name">
                            Showroom Hải Linh Long Biên
                        </div>
                        <div class="address">
                            Địa chỉ: Số 72 Đường Chu Huy Mân, Q.Long Biên, TP.Hà Nội (Hướng đi từ Cầu Vĩnh Tuy đi
                            Vinhomes Riverside Long Biên)
                        </div>
                        <div class="other">
                            <div>
                                Điện thoại: <a href="tel:0961982727">0961982727</a> -<a href="tel: 02435682266">
                                02435682266</a>
                            </div>
                        </div>
                        <div class="bt-buttom">
                            <a href="https://hailinh.vn/he-thong-cua-hang.html" title="Bản đồ">
                                <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                                c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                                C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                                s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z">
                                            </path>
                                        </g>
                                    </g>
					            </svg>
                                Bản đồ
                            </a>
                            <a href="https://www.google.com/maps?ll=21.038797,105.903821&amp;z=17&amp;t=m&amp;hl=vi&amp;gl=US&amp;mapclient=embed&amp;cid=5306855255456043534" title="Chỉ đường">
                                <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512">
                                    <path d="m8.75 17.612v4.638c0 .324.208.611.516.713.077.025.156.037.234.037.234 0 .46-.11.604-.306l2.713-3.692z"></path>
                                    <path d="m23.685.139c-.23-.163-.532-.185-.782-.054l-22.5 11.75c-.266.139-.423.423-.401.722.023.3.222.556.505.653l6.255 2.138
                                        13.321-11.39-10.308 12.419 10.483 3.583c.078.026.16.04.242.04.136 0 .271-.037.39-.109.19-.116.319-.311.352-.53l2.75-18.5c.041-.28-.077-.558-.307-.722z">
                                    </path>
                                </svg>
                                Chỉ đường
                            </a>
                        </div>
                    </div>
                    <div class="region_2 regions ">
                        <div class="name">
                            Showroom Hải Linh Hà Đông
                        </div>
                        <div class="address">
                            Địa chỉ: Ô số 5 và 6 Shophouse Đường Tố Hữu, Q.Hà Đông, TP.Hà Nội (Ngã tư Tố Hữu - Vạn Phúc)
                        </div>
                        <div class="other">
                            <div>
                                Điện thoại: <a href="tel:0968121717">0968121717</a> - <a href="tel:02435570999">02435570999</a>
                            </div>
                        </div>
                        <div class="bt-buttom">
                            <a href="https://hailinh.vn/he-thong-cua-hang.html" title="Bản đồ">
                                <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                                c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                                C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                                s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z">
                                            </path>
                                        </g>
                                    </g>
					            </svg>
                                Bản đồ
                            </a>
                            <a href="https://www.google.com/maps?ll=21.038797,105.903821&amp;z=17&amp;t=m&amp;hl=vi&amp;gl=US&amp;mapclient=embed&amp;cid=5306855255456043534" title="Chỉ đường">
                                <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512">
                                    <path d="m8.75 17.612v4.638c0 .324.208.611.516.713.077.025.156.037.234.037.234 0 .46-.11.604-.306l2.713-3.692z"></path>
                                    <path d="m23.685.139c-.23-.163-.532-.185-.782-.054l-22.5 11.75c-.266.139-.423.423-.401.722.023.3.222.556.505.653l6.255 2.138
                                        13.321-11.39-10.308 12.419 10.483 3.583c.078.026.16.04.242.04.136 0 .271-.037.39-.109.19-.116.319-.311.352-.53l2.75-18.5c.041-.28-.077-.558-.307-.722z">
                                    </path>
                                </svg>
                                Chỉ đường
                            </a>
                        </div>
                    </div>
                    <div class="region_3 regions ">
                        <div class="name">
                            Showroom Hải Linh Đống Đa
                        </div>
                        <div class="address">
                            Địa chỉ: Số 532 Đường Láng, Q. Đống Đa, TP. Hà Nội (Hướng đi từ Ngã tư sở đi Cầu Giấy)
                        </div>
                        <div class="other">
                            <div>
                                Điện thoại: <a href="tel:0936552266">0936552266</a> - <a href="tel:02438586655">02438586655</a>
                            </div>
                        </div>
                        <div class="bt-buttom">
                            <a href="https://hailinh.vn/he-thong-cua-hang.html" title="Bản đồ">
                                <svg x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                                c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                                C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                                s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z">
                                            </path>
                                        </g>
                                    </g>
					            </svg>
                                Bản đồ
                            </a>
                            <a href="https://www.google.com/maps?ll=21.038797,105.903821&amp;z=17&amp;t=m&amp;hl=vi&amp;gl=US&amp;mapclient=embed&amp;cid=5306855255456043534" title="Chỉ đường">
                                <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512">
                                    <path d="m8.75 17.612v4.638c0 .324.208.611.516.713.077.025.156.037.234.037.234 0 .46-.11.604-.306l2.713-3.692z"></path>
                                    <path d="m23.685.139c-.23-.163-.532-.185-.782-.054l-22.5 11.75c-.266.139-.423.423-.401.722.023.3.222.556.505.653l6.255 2.138
                                        13.321-11.39-10.308 12.419 10.483 3.583c.078.026.16.04.242.04.136 0 .271-.037.39-.109.19-.116.319-.311.352-.53l2.75-18.5c.041-.28-.077-.558-.307-.722z">
                                    </path>
                                </svg>
                                Chỉ đường
                            </a>
                        </div>
                    </div>
                    <div class="regions regions_last">
                        <div class="colum_4_it">
                            VPGD: Tầng 3 tòa CT2, Coma 6,N Từ Liêm, HN
                        </div>
                        <div class="colum_4_it">
                            Kho hàng: Khu Công nghiệp Phú Minh, Phường Cổ Nhuế 2, Quận Bắc Từ Liêm, HN
                        </div>
                        <div class="colum_4_it">
                            Hotline:
                        </div>
                        <div class="colum_4_it">
                            Email: info@hailinh.com.vn
                        </div>
                        <div class="colum_4_it">
                            Thời gian làm việc: 8h00 - 18h30 các ngày trong tuần
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="bg_footer_img">
    <div class="container">
        <div class="top-ft cls">
            <div class="top-ft-l">
                <ul class="menu-bottom">
                    <li class="  level0  first-item">
                        <span class="click-mobile" data-id="menu-sub1"></span> <span data-id="item_1">Hỗ trợ khách hàng</span>
                        <ul id="menu-sub1">
                            <li class="  level1  first-sitem ">
                                <a href="https://hailinh.vn/ct-chinh-sach-doi-tra-hang.html"
                                   title="Chính Sách Đổi Trả Hàng"> Chính Sách Đổi Trả Hàng </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-chinh-sach-bao-hanh-bao-tri.html"
                                   title="Chính Sách Bảo Hành/Bảo Trì"> Chính Sách Bảo Hành/Bảo Trì </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-chinh-sach-van-chuyen.html"
                                   title="Chính Sách Vận Chuyển"> Chính Sách Vận Chuyển </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-hinh-thuc-thanh-toan.html" title="Hình thức thanh toán">
                                    Hình thức thanh toán </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-chinh-sach-ban-hang-va-chat-luong-hang-hoa.html"
                                   title="Chính sách bán hàng"> Chính sách bán hàng </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-huong-dan-dat-hang.html"
                                   title="Hướng dẫn đăng ký tài khoản"> Hướng dẫn đăng ký tài khoản </a>
                            </li>
                        </ul>
                    </li>
                    <li class="  level0 menu-item">
                        <span class="click-mobile" data-id="menu-sub8"></span> <a
                            href="https://hailinh.vn/ct-gioi-thieu-ve-cong-ty-tnhh-kdtm-hai-linh.html"
                            title="Về Hải Linh"> Về Hải Linh </a>
                        <ul id="menu-sub8">
                            <li class="  level1  first-sitem ">
                                <a href="https://hailinh.vn/ct-gioi-thieu-ve-cong-ty-tnhh-kdtm-hai-linh.html"
                                   title="Giới thiệu về Hải Linh"> Giới thiệu về Hải Linh </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-dieu-khoan-su-dung.html" title="Điều khoản sử dụng"> Điều
                                    khoản sử dụng </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/ct-chinh-sach-bao-mat.html" title="Chính sách bảo mật">
                                    Chính sách bảo mật </a>
                            </li>
                        </ul>
                    </li>
                    <li class="  level0 menu-item">
                        <span class="click-mobile" data-id="menu-sub12"></span> <span data-id="item_12">		Thông tin 	</span>
                        <ul id="menu-sub12">
                            <li class="  level1  first-sitem ">
                                <a href="https://hailinh.vn/video.html" title="Videos"> Videos </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/lien-he.html" title="Liên hệ"> Liên hệ </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/tin-hai-linh-kn.html" title="Tin nội bộ"> Tin nội bộ </a>
                            </li>
                            <li class="  level1  mid-sitem ">
                                <a href="https://hailinh.vn/he-thong-cua-hang.html" title="Hệ thống cửa hàng"> Hệ thống
                                    cửa hàng </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="top-ft-c">
                <div class="title">
                    Cách thức thanh toán
                </div>
                <img width="128" height="39" src=" https://hailinh.vn/images/config/thanhtoan_1606785351.png" alt="Cách thức thanh toán">
            </div>
            <div class="top-ft-r">
                <div class="title">
                    Chứng nhận
                </div>
                <a href="//www.dmca.com/Protection/Status.aspx?ID=73e61e9f-304d-4e35-bf8a-c7d883cce82a" title="Chứng nhận bộ công thương">
                    <img class="dmca" src=" https://hailinh.vn/images/config/_dmca_premi_badge_2_1590034644.png" alt="Chứng nhận DMCA">
                </a>
                <a href="http://online.gov.vn/Home/WebDetails/74861" title="Chứng nhận bộ công thương">
                    <img class="bct" src=" https://hailinh.vn/images/config/dathongbao_1590034607.png" alt="Chứng nhận bộ công thương">
                </a>
            </div>
        </div>
    </div>
    <div class="bot-ft cls">
        <div class="container">
            <div class="bot-ft-l">
                <p>© 2007&nbsp;Công ty TNHH Kinh Doanh Thương Mại Hải Linh</p>
                <p><span style="font-size:12px">Giấy chứng nhận Đăng ký kinh doanh số 0102255737 do Sở&nbsp;kế hoạch và Đầu tư Hà Nội cấp đổi lần thứ 5&nbsp;ngày 17/9/2020</span>
                </p>
            </div>
            <div class="bot-ft-r">
                <div class="title_share">Kết nối với chúng tôi</div>
                <div class="share_fast_small">
                    <div class="fb share_item">
                        <a class="facebook-icon" href="https://www.facebook.com/hailinhgroup.com.vn" title="Link Facebook" rel="nofollow" target="_blink">
                        <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512" class="svg-inline--fa fa-facebook-f fa-w-9" style="background: #0d3a80;">
                            <path fill="#fff" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5
                                70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229" class="">
                            </path>
                        </svg>
                    </a>
                    </div>
                    <div class="zl share_item">
                        <a class="zalo-icon" href="https://oa.zalo.me/3691930773317319830" title="Link zalo"
                           rel="nofollow" target="_blink">
                            <svg x="0px" y="0px" viewBox="0 0 512.007 512.007" style="enable-background:new 0 0 512.007 512.007;" xml:space="preserve">
                                <circle style="fill:#E6EFF4;" cx="256.003" cy="256.003" r="256.003"></circle>
                                <path style="fill:#B6D1DD;" d="M385.581,107.256L385.581,107.256c-5.101-5.102-12.148-8.258-19.932-8.258H146.354
                                    c-15.567,0-28.187,12.619-28.187,28.187v219.295c0,7.785,3.156,14.832,8.258,19.933l0,0l145.105,145.105
                                    C405.682,503.489,512.001,392.169,512.001,256c0-8.086-0.393-16.081-1.126-23.976L385.581,107.256z">
                                </path>
                                <path style="fill:#41A0D7;" d="M365.647,98.999H146.353c-15.567,0-28.187,12.619-28.187,28.187v219.294
                                    c0,15.567,12.619,28.187,28.187,28.187h43.971v38.334l53.377-38.334h121.946c15.567,0,28.187-12.619,28.187-28.187V127.185
                                    C393.834,111.618,381.215,98.999,365.647,98.999z">
                                </path>
                                <path style="fill:#FFFFFF;" d="M393.834,340.942v-44.17c-5.73-5.85-13.714-9.484-22.55-9.484h-64.188l86.738-118.175V131.24
                                    c-4.466-3.988-10.304-6.31-16.5-6.31h-131.2c-17.435,0-31.57,14.135-31.57,31.57s14.135,31.57,31.57,31.57h55.168L212,311.089
                                    c-5.474,7.539-6.255,17.512-2.024,25.812c4.231,8.3,12.76,13.526,22.077,13.526h139.232
                                    C380.121,350.426,388.104,346.792,393.834,340.942z">
                                </path>
		                    </svg>
                        </a>
                    </div>
                    <div class="yt share_item">
                        <a class="youtube-icon" href="https://www.youtube.com/channel/UCjkONjs91TnYu25zS6uVflg"
                           title="Link youtube" rel="nofollow" target="_blink">
                            <svg x="0px" y="0px" viewBox="0 0 90.677 90.677"
                                 style="enable-background:new 0 0 90.677 90.677;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M82.287,45.907c-0.937-4.071-4.267-7.074-8.275-7.521c-9.489-1.06-19.098-1.065-28.66-1.06
                                                c-9.566-0.005-19.173,0-28.665,1.06c-4.006,0.448-7.334,3.451-8.27,7.521c-1.334,5.797-1.35,12.125-1.35,18.094
                                                c0,5.969,0,12.296,1.334,18.093c0.936,4.07,4.264,7.073,8.272,7.521c9.49,1.061,19.097,1.065,28.662,1.061
                                                c9.566,0.005,19.171,0,28.664-1.061c4.006-0.448,7.336-3.451,8.272-7.521c1.333-5.797,1.34-12.124,1.34-18.093
                                                C83.61,58.031,83.62,51.704,82.287,45.907z M28.9,50.4h-5.54v29.438h-5.146V50.4h-5.439v-4.822H28.9V50.4z M42.877,79.839h-4.629
                                                v-2.785c-1.839,2.108-3.585,3.136-5.286,3.136c-1.491,0-2.517-0.604-2.98-1.897c-0.252-0.772-0.408-1.994-0.408-3.796V54.311
                                                h4.625v18.795c0,1.084,0,1.647,0.042,1.799c0.111,0.718,0.462,1.082,1.082,1.082c0.928,0,1.898-0.715,2.924-2.166v-19.51h4.629
                                                L42.877,79.839L42.877,79.839z M60.45,72.177c0,2.361-0.159,4.062-0.468,5.144c-0.618,1.899-1.855,2.869-3.695,2.869
                                                c-1.646,0-3.234-0.914-4.781-2.824v2.474h-4.625V45.578h4.625v11.189c1.494-1.839,3.08-2.769,4.781-2.769
                                                c1.84,0,3.078,0.969,3.695,2.88c0.311,1.027,0.468,2.715,0.468,5.132V72.177z M77.907,67.918h-9.251v4.525
                                                c0,2.363,0.773,3.543,2.363,3.543c1.139,0,1.802-0.619,2.066-1.855c0.043-0.251,0.104-1.279,0.104-3.134h4.719v0.675
                                                c0,1.491-0.057,2.518-0.099,2.98c-0.155,1.024-0.519,1.953-1.08,2.771c-1.281,1.854-3.179,2.768-5.595,2.768
                                                c-2.42,0-4.262-0.871-5.599-2.614c-0.981-1.278-1.485-3.29-1.485-6.003v-8.941c0-2.729,0.447-4.725,1.43-6.015
                                                c1.336-1.747,3.177-2.617,5.54-2.617c2.321,0,4.161,0.87,5.457,2.617c0.969,1.29,1.432,3.286,1.432,6.015v5.285H77.907z"></path>
                                            <path d="M70.978,58.163c-1.546,0-2.321,1.181-2.321,3.541v2.362h4.625v-2.362C73.281,59.344,72.508,58.163,70.978,58.163z"></path>
                                            <path d="M53.812,58.163c-0.762,0-1.534,0.36-2.307,1.125v15.559c0.772,0.774,1.545,1.14,2.307,1.14
                                                c1.334,0,2.012-1.14,2.012-3.445V61.646C55.824,59.344,55.146,58.163,53.812,58.163z"></path>
                                            <path d="M56.396,34.973c1.705,0,3.479-1.036,5.34-3.168v2.814h4.675V8.82h-4.675v19.718c-1.036,1.464-2.018,2.188-2.953,2.188
                                                c-0.626,0-0.994-0.37-1.096-1.095c-0.057-0.153-0.057-0.722-0.057-1.817V8.82h-4.66v20.4c0,1.822,0.156,3.055,0.414,3.836
                                                C53.854,34.363,54.891,34.973,56.396,34.973z"></path>
                                            <path d="M23.851,20.598v14.021h5.184V20.598L35.271,0h-5.242l-3.537,13.595L22.812,0h-5.455c1.093,3.209,2.23,6.434,3.323,9.646
                                                C22.343,14.474,23.381,18.114,23.851,20.598z"></path>
                                            <path d="M42.219,34.973c2.342,0,4.162-0.881,5.453-2.641c0.981-1.291,1.451-3.325,1.451-6.067v-9.034
                                                c0-2.758-0.469-4.774-1.451-6.077c-1.291-1.765-3.11-2.646-5.453-2.646c-2.33,0-4.149,0.881-5.443,2.646
                                                c-0.993,1.303-1.463,3.319-1.463,6.077v9.034c0,2.742,0.47,4.776,1.463,6.067C38.069,34.092,39.889,34.973,42.219,34.973z
                                                 M39.988,16.294c0-2.387,0.724-3.577,2.231-3.577c1.507,0,2.229,1.189,2.229,3.577v10.852c0,2.387-0.722,3.581-2.229,3.581
                                                c-1.507,0-2.231-1.194-2.231-3.581V16.294z"></path>
                                        </g>
                                    </g>
                            </svg>
                        </a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="fixed-bar">
    <div id="bar-inner">
        <a class="go-top" href="#page-wrapper" title="Back to top">
            <svg x="0px" y="0px" viewBox="0 0 284.929 284.929" style="enable-background:new 0 0 284.929 284.929;" xml:space="preserve">
                <g>
                    <path d="M282.082,195.285L149.028,62.24c-1.901-1.903-4.088-2.856-6.562-2.856s-4.665,0.953-6.567,2.856L2.856,195.285
                        C0.95,197.191,0,199.378,0,201.853c0,2.474,0.953,4.664,2.856,6.566l14.272,14.271c1.903,1.903,4.093,2.854,6.567,2.854
                        c2.474,0,4.664-0.951,6.567-2.854l112.204-112.202l112.208,112.209c1.902,1.903,4.093,2.848,6.563,2.848
                        c2.478,0,4.668-0.951,6.57-2.848l14.274-14.277c1.902-1.902,2.847-4.093,2.847-6.566
                        C284.929,199.378,283.984,197.188,282.082,195.285z">
                    </path>
                </g>
            </svg>
        </a>
    </div>
</div>
<div class="megamenu_mb">
    <div class="close_all">
        <div class="navicon-line"></div>
        <div class="navicon-line"></div>
    </div>
    <ul class="menu scroll_bar">
        <div class="label" id="menu_">Menu</div>
        <li class="group_id_menu_23 level_0">
            <a href="https://hailinh.vn/gach-op-lat-pc83.html">Gạch ốp lát</a>
            <span class="next_menu" id="next_1036"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1036">
                <div class="label" id="close_1036">Gạch ốp lát</div>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn//">Thương hiệu</a>
                    <span class="next_menu" id="next_1067"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1067">
                        <div class="label" id="close_1067">Thương hiệu</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-viglacera-pcm83.html">Gạch Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-taicera-pcm83.html">Gạch Taicera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-dong-tam-pcm83.html">Gạch Đồng Tâm</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-mosaic-pcm83.html">Gạch Mosaic</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-tay-ban-nha-pcm83.html">Gạch Tây Ban Nha</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-an-do-pcm83.html">Gạch Ấn Độ</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-y-my-pcm83.html">Gạch Ý Mỹ</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-cnc-pc344.html">Gạch CNC</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn/gach-op-tuong-pc150.html">Gạch ốp tường</a>
                    <span class="next_menu" id="next_1040"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1040">
                        <div class="label" id="close_1040">Gạch ốp tường</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-12x36-pc230.html">Gạch ốp tường 12x36</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-15x45-pc227.html">Gạch ốp tường 15x45</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-15x60-pc242.html">Gạch ốp tường 15x60</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-20x80-pc167.html">Gạch ốp tường 20x80</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-30x30-pc221.html">Gạch ốp tường 30x30</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-30x60-pc161.html">Gạch ốp tường 30x60</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-40x80-pc152.html">Gạch ốp tường 40x80</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-30x90-pc273.html">Gạch ốp tường 30x90</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-20x20-pc348.html">Gạch ốp tường 20x20</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-tuong-kich-thuoc-khac-pc164.html">Kích thước khác</a>
                        </li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn/gach-lat-nen-pc151.html">Gạch lát nền</a>
                    <span class="next_menu" id="next_1041"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1041">
                        <div class="label" id="close_1041">Gạch lát nền</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-30x30-pc154.html">Gạch lát nền 30x30</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-40x40-pc169.html">Gạch lát nền 40x40</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-50x50-pc171.html">Gạch lát nền 50x50</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-60x60-pc156.html">Gạch lát nền 60x60</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-75x75-pc157.html">Gạch lát nền 75x75</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-80x80-pc158.html">Gạch lát nền 80x80</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-30x60-pc165.html">Gạch lát nền 30x60</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-60x120-pc155.html">Gạch lát nền 60x120</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-20x20-pc350.html">Gạch lát nền 20x20</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-lat-nen-kich-thuoc-khac-pc160.html">Kích thước khác</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn//">Bộ sưu tập</a>
                    <span class="next_menu" id="next_1125"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1125">
                        <div class="label" id="close_1125">Bộ sưu tập</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-trang-tri-dlt.html">Gạch trang trí</a> </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-van-da-dlt.html">Gạch vân đá</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-van-may-dlt.html">Gạch vân mây</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-gia-xi-mang-dlt.html">Gạch giả xi măng</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-mot-mau-dlt.html">Gạch một màu</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-bong-dlt.html">Gạch bông</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn//">Công năng gạch</a>
                    <span class="next_menu" id="next_1133"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1133">
                        <div class="label" id="close_1133">Công năng gạch</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-op-lat-ngoai-troi-dlt.html">Gạch ốp lát ngoài trời</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-op-lat-trong-nha-dlt.html">Gạch ốp lát trong nhà</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-op-lat-be-boi-dlt.html">Gạch ốp bể bơi</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn//">Xuất xứ</a>
                    <span class="next_menu" id="next_1137"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1137">
                        <div class="label" id="close_1137">Xuất xứ</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-nhap-khau-dlt.html">Gạch nhập khẩu</a> </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gach-op-lat-pc83/gach-trong-nuoc-dlt.html">Việt Nam</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0">
            <a href="https://hailinh.vn//ngoi-lop-mai-nha-pc352.html">Ngói lợp mái nhà</a><span class="next_menu" id="next_1302"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1302">
                <div class="label" id="close_1302">Ngói lợp mái nhà</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn//">Thương hiệu</a><span class="next_menu" id="next_1303"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1303">
                        <div class="label" id="close_1303">Thương hiệu</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-viglacera-pcm352.html">Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-dat-viet-pcm352.html">Đất Việt</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-prime-pcm352.html">Prime</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1">
                    <a href="https://hailinh.vn//">Kiểu ngói</a>
                    <span class="next_menu"id="next_1307"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1307">
                        <div class="label" id="close_1307">Kiểu ngói</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-phang-dlt.html">Ngói phẳng</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-song-dlt.html">Ngói sóng</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-mui-dlt.html">Ngói mũi</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ngoi-lop-mai-nha-pc352/ngoi-noc-dlt.html">Ngói nóc</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0"><a href="https://hailinh.vn/thiet-bi-ve-sinh-pc73.html">Thiết bị vệ
            sinh</a><span class="next_menu" id="next_1037"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1037">
                <div class="label" id="close_1037">Thiết bị vệ sinh</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bon-cau-pc74.html">Bồn cầu</a><span
                        class="next_menu" id="next_1071"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1071">
                        <div class="label" id="close_1071">Bồn cầu</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-inax-pcm74.html">Bồn
                            cầu Inax</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-viglacera-pcm74.html">Bồn
                            cầu Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-toto-pcm74.html">Bồn
                            cầu TOTO</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-1-khoi-pc99.html">Bồn
                            cầu 1 khối</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-2-khoi-pc100.html">Bồn
                            cầu 2 khối</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-thong-minh-pc102.html">Bồn
                            cầu thông minh</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-cau-am-tuong-pc101.html">Bồn
                            cầu âm tường</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/nap-bon-cau-pc103.html">Nắp bồn
                            cầu</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/chau-rua-pc75.html">Chậu rửa</a><span
                        class="next_menu" id="next_1077"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1077">
                        <div class="label" id="close_1077">Chậu rửa</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/chau-rua-inax-pcm75.html">Chậu
                            rửa Inax</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/chau-rua-viglacera-pcm75.html">Chậu
                            rửa Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/chau-rua-toto-pcm75.html">Chậu
                            rửa TOTO</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/chau-rua-dat-ban-pc104.html">Chậu
                            rửa đặt bàn</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/chau-rua-treo-tuong-pc105.html">Chậu rửa treo tường</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bo-tu-chau-pc231.html">Bộ tủ
                            chậu</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phu-kien-chau-rua-pc109.html">Phụ
                            kiện chậu rửa</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bon-tam-pc79.html">Bồn tắm</a><span
                        class="next_menu" id="next_1094"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1094">
                        <div class="label" id="close_1094">Bồn tắm</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tam-belli-pcm79.html">Bồn
                            tắm Belli</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tam-inax-pcm79.html">Bồn
                            tắm Inax</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tam-toto-pcm79.html">Bồn
                            tắm TOTO</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tam-thuong-pc119.html">Bồn
                            tắm thường</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tam-massage-pc122.html">Bồn
                            tắm massage</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phu-kien-bon-tam-pc124.html">Phụ
                            kiện bồn tắm</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phong-xong-hoi-pc80.html">Phòng xông
                    hơi</a><span class="next_menu" id="next_1098"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1098">
                        <div class="label" id="close_1098">Phòng xông hơi</div>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-xong-hoi-belli-pcm80.html">Phòng xông hơi Belli</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-xong-hoi-govern-pcm80.html">Phòng xông hơi Govern</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-xong-hoi-nofer-pcm80.html">Phòng xông hơi Nofer</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-xong-hoi-euroking-pcm80.html">Phòng xông hơi Euroking</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-xong-hoi-appollo-pcm80.html">Phòng xông hơi Appollo</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phong-xong-hoi-pc80.html">Phòng
                            xông hơi Fantiny</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phu-kien-nha-tam-pc82.html">Phụ kiện
                    nhà tắm</a><span class="next_menu" id="next_1101"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1101">
                        <div class="label" id="close_1101">Phụ kiện nhà tắm</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bo-phu-kien-pc143.html">Bộ phụ
                            kiện</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/guong-pc125.html">Gương</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/hop-nuoc-hoa-pc141.html">Hộp
                            nước hoa</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/hop-dung-xa-phong-pc208.html">Hộp
                            đựng xà phòng</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ke-de-do-pc126.html">Kệ để
                            đồ</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/lo-giay-pc140.html">Lô giấy</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/may-say-tay-pc139.html">Máy sấy
                            tay</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/moc-ao-pc136.html">Móc áo</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/voi-xit-pc138.html">Vòi xịt</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/tay-vin-nha-tam-pc144.html">Tay
                            vịn nhà tắm</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/xi-phong-pc196.html">Xi
                            phông</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/thoat-san-pc137.html">Thoát
                            sàn</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/treo-khan-pc132.html">Treo
                            khăn</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/vach-ngan-pc135.html">Vách
                            ngăn</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phu-kien-khac-pc142.html">Phụ
                            kiện khác</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/sen-tam-pc76.html">Sen tắm</a><span
                        class="next_menu" id="next_1085"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1085">
                        <div class="label" id="close_1085">Sen tắm</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-belli-pcm76.html">Sen
                            tắm Belli</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-inax-pcm76.html">Sen
                            tắm Inax</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-viglacera-pcm76.html">Sen
                            tắm Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-thuong-pc110.html">Sen
                            tắm thường</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-toto-pcm76.html">Sen
                            tắm TOTO</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-cay-pc111.html">Sen tắm
                            cây</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/sen-tam-am-tuong-pc112.html">Sen
                            tắm âm tường</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phu-kien-sen-tam-pc113.html">Phụ
                            kiện sen tắm</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phong-tam-kinh-pc81.html">Phòng tắm
                    kính</a><span class="next_menu" id="next_1100"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1100">
                        <div class="label" id="close_1100">Phòng tắm kính</div>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-tam-kinh-belli-pcm81.html">Phòng tắm kính Belli</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-tam-kinh-euroking-pcm81.html">Phòng tắm kính Euroking</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-tam-kinh-caesar-pcm81.html">Phòng tắm kính Caesar</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-tam-kinh-fendi-pcm81.html">Phòng tắm kính Fendi</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-tam-kinh-fantiny-pcm81.html">Phòng tắm kính Fantiny</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/phong-tam-kinh-govern-pcm81.html">Phòng tắm kính Govern</a>
                        </li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/voi-chau-pc114.html">Vòi chậu</a><span
                        class="next_menu" id="next_1082"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1082">
                        <div class="label" id="close_1082">Vòi chậu</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/voi-chau-belli-pcm114.html">Vòi
                            chậu Belli</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/voi-chau-inax-pcm114.html">Vòi
                            chậu Inax</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/voi-chau-viglacera-pcm114.html">Vòi chậu Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/voi-chau-toto-pcm114.html">Vòi
                            chậu TOTO</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/voi-chau-nong-lanh-pc210.html">Vòi
                            chậu nóng lanh</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/voi-chau-1-duong-nuoc-pc211.html">Vòi chậu 1 đường lạnh</a>
                        </li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bon-tieu-pc78.html">Bồn tiểu</a><span
                        class="next_menu" id="next_1090"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1090">
                        <div class="label" id="close_1090">Bồn tiểu</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tieu-inax-pcm78.html">Bồn
                            tiểu Inax</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tieu-viglacera-pcm78.html">Bồn
                            tiểu Viglacera</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tieu-toto-pcm78.html">Bồn
                            tiểu TOTO</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tieu-nam-pc116.html">Bồn
                            tiểu nam</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-tieu-nu-pc117.html">Bồn
                            tiểu nữ</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phu-kien-bon-tieu-pc118.html">Phụ
                            kiện bồn tiểu</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0"><a href="https://hailinh.vn/do-gia-dung-pc181.html">Đồ gia dụng</a><span
                class="next_menu" id="next_1039"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1039">
                <div class="label" id="close_1039">Đồ gia dụng</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-xay-sinh-to-pc254.html">Máy xay
                    sinh tố</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/noi-chien-khong-dau-pc256.html">Nồi
                    Chiên không dầu</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-loc-khong-khi-pc258.html">Máy lọc
                    không khí</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/binh-dun-sieu-toc-pc259.html">Bình đun
                    siêu tốc</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/lo-vi-song-pc260.html">Lò vi sóng</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-ep-trai-cay-pc261.html">Máy ép trái
                    cây</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/chao-pc267.html">Chảo</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/noi-pc268.html">Nồi</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-pha-cafe-pc269.html">Máy pha
                    cafe</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-suoi-pc272.html">Máy sưởi</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/am-dun-nuoc-pc302.html">Ấm đun nước</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phu-kien-nha-bep-pc274.html">Phụ kiện
                    nhà bếp</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/tu-ruou-pc275.html">Tủ rượu</a></li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0"><a href="https://hailinh.vn/noi-that-pc281.html">Nội thất</a><span
                class="next_menu" id="next_1111"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1111">
                <div class="label" id="close_1111">Nội thất</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phu-kien-cua-pc307.html">Phụ kiện
                    cửa</a><span class="next_menu" id="next_1223"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1223">
                        <div class="label" id="close_1223">Phụ kiện cửa</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/tay-nam-cua-pc308.html">Tay nắm
                            cửa</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/ban-le-cua-pc312.html">Bản lề
                            cửa</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/thiet-bi-dong-cua-tu-dong-pc313.html">Thiết bị đóng cửa tự
                            động</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/chot-chan-cua-pc314.html">Chốt
                            chặn cửa</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/phu-kien-cua-khac-pc309.html">Phụ
                            kiện cửa khác</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/chan-ga-goi-dem-pc200.html">Chăn ga gối
                    đệm</a><span class="next_menu" id="next_1174"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1174">
                        <div class="label" id="close_1174">Chăn ga gối đệm</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bo-chan-ga-goi-pc201.html">Bộ
                            chăn ga gối</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/chan-ga-goi-dem-everon-pcm200.html">Chăn ga gối đệm Everon</a>
                        </li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/san-nhua-pc278.html">Sàn nhựa</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/san-go-pc282.html">Sàn gỗ</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/quat-tran-pc294.html">Quạt trần</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/khoa-cua-pc303.html">Khóa cửa</a><span
                        class="next_menu" id="next_1219"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1219">
                        <div class="label" id="close_1219">Khóa cửa</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/khoa-dien-tu-pc304.html">Khóa
                            điện tử</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn//">Khóa tay nắm cửa</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn//">Phụ kiện khóa</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0"><a href="https://hailinh.vn/bon-binh-pc285.html">Bồn bình</a><span
                class="next_menu" id="next_1110"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1110">
                <div class="label" id="close_1110">Bồn bình</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/binh-nong-lanh-pc286.html">Bình nóng
                    lạnh</a><span class="next_menu" id="next_1179"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1179">
                        <div class="label" id="close_1179">Bình nóng lạnh</div>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/truc-tiep-pc287.html">Bình nóng
                            lạnh trực tiếp</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/gian-tiep-pc288.html">Bình nóng
                            lạnh gián tiếp</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/binh-nong-lanh-ariston-pcm286.html">Bình nóng lạnh Ariston</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/binh-nong-lanh-ferroli-pcm286.html">Bình nóng lạnh Ferroli</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-binh-ferroli-pcm285.html">Bình
                            nóng lạnh Picenza</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/binh-nong-lanh-rossi-pcm286.html">Bình nóng lạnh Rossi</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/binh-nong-lanh-son-ha-pcm286.html">Bình nóng lạnh Sơn Hà</a>
                        </li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bon-nuoc-pc236.html">Bồn chứa
                    nước</a><span class="next_menu" id="next_1180"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1180">
                        <div class="label" id="close_1180">Bồn chứa nước</div>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/bon-nuoc-pc236/bon-nuoc-bon-dung-dlt.html">Bồn đứng</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/bon-nuoc-pc236/bon-nuoc-bon-ngang-dlt.html">Bồn ngang</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-nuoc-tan-a-pcm236.html">Bồn
                            nước Tân Á</a></li>
                        <li class="group_id_menu_23 level_2"><a href="https://hailinh.vn/bon-nuoc-son-ha-pcm236.html">Bồn
                            nước Sơn Hà</a></li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a
                        href="https://hailinh.vn/binh-nuoc-nong-nang-luong-mat-troi-pc289.html">Bình nước nóng năng
                    lượng mặt trời</a><span class="next_menu" id="next_1181"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1181">
                        <div class="label" id="close_1181">Bình nước nóng năng lượng mặt trời</div>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/binh-nuoc-nong-nang-luong-mat-troi-tan-a-pcm289.html">Bình nước
                            nóng năng lượng mặt trời Tân Á</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/binh-nuoc-nong-nang-luong-mat-troi-son-ha-pcm289.html">Bình
                            nước nóng năng lượng mặt trời Sơn Hà</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0"><a href="https://hailinh.vn/thiet-bi-nha-bep-pc291.html">Thiết bị nhà
            bếp</a><span class="next_menu" id="next_1142"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1142">
                <div class="label" id="close_1142">Thiết bị nhà bếp</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/voi-bep-pc115.html">Vòi bếp</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bep-tu-pc245.html">Bếp từ</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bep-dien-tu-pc249.html">Bếp điện từ</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/bep-ga-pc255.html">Bếp ga</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/lo-nuong-pc257.html">Lò nướng</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-hut-mui-pc266.html">Máy hút mùi</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-rua-bat-pc270.html">Máy rửa bát</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/noi-chao-pc293.html">Nồi chảo</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phu-kien-phong-bep-pc292.html">Phụ kiện
                    nhà bếp</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/chau-rua-bat-pc232.html">Chậu rửa
                    bát</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-loc-nuoc-pc251.html">Máy lọc
                    nước</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/may-say-pc315.html">Máy sấy bát</a>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_23 level_0"><a href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-pc214.html">Hóa chất
            phụ gia xây dựng</a><span class="next_menu" id="next_1112"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1112">
                <div class="label" id="close_1112">Hóa chất phụ gia xây dựng</div>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/keo-dan-gach-pc276.html">Keo dán
                    gạch</a><span class="next_menu" id="next_1191"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1191">
                        <div class="label" id="close_1191">Keo dán gạch</div>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/keo-dan-gach-trong-nha-pc215.html">Keo dán gạch trong nhà</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/keo-dan-gach-ngoai-troi-pc216.html">Keo dán gạch ngoài trời</a>
                        </li>
                    </ul>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/keo-cha-ron-pc217.html">Keo chà ron</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/chong-tham-dan-dung-pc218.html">Chống
                    thấm dân dụng</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/tay-rua-xu-ly-vet-ban-pc219.html">Tẩy
                    rửa sử lý vết bẩn</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/phu-gia-chat-ket-dinh-pc277.html">Phụ
                    gia , chất kết dính</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/danh-bong-pc220.html">Đánh bóng</a>
                </li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/vua-chuyen-dung-pc280.html">Vữa chuyên
                    dụng</a></li>
                <li class="group_id_menu_23 level_1"><a href="https://hailinh.vn/thuong-hieu.html">Thương hiệu sản
                    phẩm</a><span class="next_menu" id="next_1277"></span>
                    <ul class="highlight highlight_2 scroll_bar" id="sub_menu_1277">
                        <div class="label" id="close_1277">Thương hiệu sản phẩm</div>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-asia-star-pcm214.html">ASIA-STAR</a>
                        </li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-webber-pcm214.html">WEBBER</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-miracle-water-pcm214.html">MIRACLE
                            WATER</a></li>
                        <li class="group_id_menu_23 level_2"><a
                                href="https://hailinh.vn/hoa-chat-phu-gia-xay-dung-sika-pcm214.html">SIKA</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="group_id_menu_2 level_0">
            <a href="https://hailinh.vn/khuyen-mai.html">
                <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 497.882 497.882" height="512" viewBox="0 0 497.882 497.882" width="512">
                    <path d="m295.617 0c-106.104 61.135-93.353 233.382-93.353 233.382s-46.676-15.559-46.676-85.573c-55.688 32.291-93.353 94.357-93.353 163.367 0 103.115 83.591 186.706
                         186.706 186.706s186.706-83.591 186.706-186.706c-.001-151.698-140.03-182.816-140.03-311.176zm-30.276 433.549c-37.518 9.354-75.517-13.477-84.873-50.997-9.354-37.518
                         13.477-75.519 50.997-84.873 90.58-22.584 101.932-73.521 101.932-73.521s45.169 181.16-68.056 209.391z">
                    </path>
                </svg>
                Khuyến mại hot
            </a>
        </li>
        <li class="group_id_menu_2 level_0">
            <a href="https://hailinh.vn/combo.html">
                <svg x="0px" y="0px" viewBox="0 0 25.999 25.999" style="enable-background:new 0 0 25.999 25.999;" xml:space="preserve">
                    <g>
                        <path d="M25.856,8.485l-3-5c-0.003-0.005-0.009-0.008-0.013-0.013c-0.015-0.023-0.031-0.044-0.048-0.066
                           c-0.029-0.038-0.062-0.073-0.096-0.107s-0.068-0.068-0.107-0.096c-0.016-0.012-0.029-0.027-0.045-0.038
                           c-0.006-0.004-0.014-0.006-0.021-0.01c-0.02-0.012-0.043-0.019-0.063-0.03c-0.028-0.015-0.051-0.036-0.081-0.049
                           c-0.015-0.006-0.031-0.006-0.046-0.011c-0.047-0.017-0.094-0.026-0.142-0.036c-0.014-0.003-0.028-0.008-0.042-0.011
                           c-0.011-0.002-0.02-0.01-0.031-0.011c-0.003,0-0.007,0.001-0.01,0.001c-0.022-0.003-0.044-0.001-0.067-0.002
                           C22.029,3.006,22.015,3,21.999,3H8.73C8.525,3,8.326,3.063,8.158,3.18l-5.731,4C2.404,7.196,2.39,7.219,2.369,7.236
                           C2.346,7.255,2.32,7.267,2.299,7.288C2.283,7.304,2.274,7.326,2.258,7.343c-0.04,0.045-0.074,0.092-0.105,0.143
                           C2.139,7.51,2.117,7.527,2.105,7.552l-2,4c-0.192,0.385-0.117,0.85,0.188,1.154C0.484,12.899,0.74,13,0.999,13
                           c0.151,0,0.305-0.034,0.447-0.105l0.553-0.277V22c0,0.553,0.448,1,1,1h15c0.147,0,0.285-0.036,0.411-0.093
                           c0.034-0.015,0.06-0.043,0.092-0.062c0.087-0.052,0.17-0.111,0.24-0.189c0.012-0.013,0.029-0.018,0.04-0.031l4-5
                           c0.14-0.178,0.217-0.397,0.217-0.625v-4.586l2.707-2.707C26.03,9.383,26.093,8.879,25.856,8.485z M9.044,5h10.541l-2,2H6.179
                           L9.044,5z M3.999,11V9h13v12h-13V11z M13.999,11c0,0.553-0.447,1-1,1h-5c-0.552,0-1-0.447-1-1c0-0.552,0.448-1,1-1h5
                           C13.552,10,13.999,10.448,13.999,11z">
                        </path>
                    </g>
                </svg>
                Combo sản phẩm
            </a>
            <span class="next_menu" id="next_1007"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1007">
                <div class="label" id="close_1007">Combo sản phẩm</div>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo337">Combo Toto +
                    Belli</a></li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo338">Combo Cotto +
                    Belli</a></li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo332">Combo
                    Viglacera</a></li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo333">Combo Inax</a>
                </li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo339">Combo ToTo</a>
                </li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo334">Combo Caesar</a>
                </li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo335">Combo Cotto</a>
                </li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/combo.html#catcombo336">Combo American
                    Standard</a></li>
            </ul>
        </li>
        <li class="group_id_menu_2 level_0">
            <a href="https://hailinh.vn/san-pham-ban-chay.html">
                <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 512.007 512.007" height="512" viewBox="0 0 512.007 512.007" width="512">
                    <g>
                        <path d="m253.997 225.923c-2.168 5.2-7.046 8.745-12.656 9.199l-5.259.41 3.999 3.428c4.277 3.662 6.138 9.404 4.834 14.883l-1.216
                             5.098 4.482-2.739c4.805-2.93 10.84-2.93 15.645 0l4.482 2.739-1.216-5.098c-1.304-5.479.557-11.221
                             4.834-14.883l3.999-3.428-5.259-.41c-5.61-.454-10.488-3.999-12.656-9.199l-2.007-4.849z">
                        </path>
                        <path d="m151.004 142.478v63.289c0 81.724 39.067 157.266 105 204.053 65.933-46.787 105-122.329 105-204.053v-63.289c-36.489-7.471-71.719-20.537-105-38.921-33.267
                         18.369-68.496 31.436-105 38.921zm118.857 33.772 12.334 29.692 32.08 2.563c6.035.483 11.191 4.556 13.066 10.313 1.875 5.771.088 12.1-4.512 16.04l-24.434 20.933
                          7.471 31.274c1.406 5.903-.864 12.056-5.771 15.615-4.824 3.504-11.34 3.887-16.641.674l-27.451-16.772-27.451
                          16.772c-5.156 3.149-11.719 2.9-16.641-.674-4.907-3.56-7.178-9.712-5.771-15.615l7.471-31.274-24.434-20.933c-4.6-3.94-6.387-10.269-4.512-16.04 1.875-5.757
                          7.031-9.829 13.066-10.313l32.08-2.563 12.334-29.692c4.659-11.191 23.058-11.191 27.716 0z">
                        </path>
                        <path d="m436.004 60.007c-87.246 0-145.474-39.653-167.402-54.58l-4.277-2.9c-5.039-3.369-11.602-3.369-16.641 0l-4.307 2.915c-23.979 16.333-80.142 54.565-167.373
                             54.565-8.291 0-15 6.709-15 15v130.76c0 129.858 72.144 246.592 188.291 304.658 2.109 1.055 4.409 1.582 6.709 1.582s4.6-.527 6.709-1.582c116.147-58.066 188.291-174.8
                             188.291-304.658v-130.76c0-8.291-6.709-15-15-15zm-45 145.76c0 95.215-47.402 182.974-126.812 234.756-2.49 1.626-5.332 2.432-8.188
                             2.432s-5.698-.806-8.188-2.432c-79.409-51.782-126.812-139.541-126.812-234.756v-75.74c0-7.324 5.288-13.564 12.495-14.795 40.195-6.797 78.867-20.889 114.961-41.88
                             4.658-2.725 10.43-2.725 15.088 0 36.123 21.006 74.795 35.098 114.946 41.88 7.222 1.23 12.51 7.471 12.51 14.795z">
                        </path>
                    </g>
                </svg>
                Sản phẩm bán chạy
            </a>
        </li>
        <li class="group_id_menu_2 level_0">
            <a href="https://hailinh.vn/bo-suu-tap.html">
                <svg xmlns="http://www.w3.org/2000/svg" height="511pt" viewBox="0 -21 511.98744 511" width="511pt">
                    <path d="m133.320312 373.828125c-34.152343
                        0-64.53125-21.867187-75.5625-54.421875l-.746093-2.453125c-2.601563-8.621094-3.691407-15.871094-3.691407-23.125v-145.453125l-51.753906 172.757812c-6.65625
                        25.410157 8.511719 51.753907 33.960938 58.773438l329.878906 88.34375c4.117188 1.066406 8.234375 1.578125 12.289062 1.578125 21.246094 0 40.660157-14.101563
                        46.101563-34.882813l19.21875-61.117187zm0 0">
                    </path>
                    <path d="m191.988281 149.828125c23.53125 0 42.664063-19.136719 42.664063-42.667969s-19.132813-42.667968-42.664063-42.667968-42.667969 19.136718-42.667969
                        42.667968 19.136719 42.667969 42.667969 42.667969zm0 0">
                    </path>
                    <path d="m458.652344.492188h-320c-29.394532 0-53.332032 23.9375-53.332032 53.335937v234.664063c0 29.398437 23.9375 53.335937 53.332032 53.335937h320c29.398437 0
                        53.335937-23.9375 53.335937-53.335937v-234.664063c0-29.398437-23.9375-53.335937-53.335937-53.335937zm-320 42.667968h320c5.890625 0 10.667968 4.777344 10.667968
                        10.667969v151.445313l-67.390624-78.636719c-7.148438-8.382813-17.496094-12.863281-28.609376-13.117188-11.050781.0625-21.417968 4.96875-28.5 13.460938l-79.234374
                        95.101562-25.8125-25.75c-14.589844-14.589843-38.335938-14.589843-52.90625 0l-58.878907 58.859375v-201.363281c0-5.890625
                        4.777344-10.667969 10.664063-10.667969zm0 0">
                    </path>
                </svg>
            Bộ sưu tập</a></li>
        <li class="group_id_menu_2 level_0">
            <a href="https://hailinh.vn/tu-van-kn.html">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="45.854px" height="45.854px" viewBox="0 0 45.854 45.854" style="enable-background:new 0 0 45.854 45.854;" xml:space="preserve">
                    <g>
                        <path d="M31.751,26.057l-3.572-0.894c1.507-1.339,2.736-3.181,3.559-5.2c1.152-0.255,2.209-1.214,2.383-3.327
                            c0.164-1.99-0.444-2.877-1.26-3.264c-0.01-0.237-0.021-0.469-0.035-0.696c1.049-5.812-3.103-7.843-3.426-7.941
                            c-0.541-0.822-4.207-5.821-10.858-1.763c-0.974,0.594-2.519,1.764-3.344,2.942c-0.992,1.246-1.713,3.049-2.009,5.655
                            c-0.205,0.022-0.409,0.054-0.611,0.1c-0.054-1.279,0.017-5.048,2.405-7.728c1.766-1.981,4.448-2.986,7.968-2.986
                            c3.526,0,6.263,1.013,8.135,3.009c3.075,3.28,2.797,8.175,2.794,8.224c-0.018,0.262,0.182,0.489,0.443,0.506
                            c0.012,0.001,0.021,0.001,0.033,0.001c0.248,0,0.457-0.193,0.475-0.445c0.015-0.217,0.312-5.345-3.045-8.933
                            C29.727,1.116,26.754,0,22.95,0c-3.808,0-6.73,1.114-8.686,3.312c-2.859,3.213-2.706,7.66-2.625,8.695
                            c-1.113,0.595-1.917,1.89-1.689,4.662c0.301,3.653,2.422,4.926,4.428,4.926c0.39,0,0.734-0.084,1.038-0.239
                            c1.148,0.743,2.991,1.387,5.944,1.507c0.29,0.436,0.896,0.735,1.6,0.735c0.982,0,1.779-0.586,1.779-1.309
                            c0-0.723-0.797-1.309-1.779-1.309c-0.801,0-1.478,0.389-1.701,0.924c-1.937-0.092-3.324-0.421-4.315-0.836
                            c-1.353-1.819-1.996-4.907-1.996-6.671c0-0.653,0.021-1.26,0.059-1.825c6.256,0.21,9.975-2.319,12.007-4.516
                            c2.617,2.608,3.548,6.138,3.858,7.825c-0.606,4.628-3.906,9.54-7.912,9.54c-1.723,0-3.316-0.909-4.62-2.313
                            c-1.332-0.278-2.441-0.713-3.323-1.303c0.72,1.287,1.614,2.438,2.645,3.352l-3.58,0.898c-4.889,1.225-8.28,5.598-8.28,10.637v6.033
                            c0,1.711,1.347,3.129,3.058,3.129h28.09c1.711,0,3.102-1.418,3.102-3.129v-6.033C40.053,31.654,36.64,27.279,31.751,26.057z
                            M30.272,27.652l-4.65,3.183l-1.416-3.622c0.64-0.12,1.259-0.33,1.853-0.617L30.272,27.652z M19.786,26.589
                            c0.583,0.28,1.194,0.488,1.827,0.61l-1.422,3.637l-4.651-3.184L19.786,26.589z M18.753,43.896h-5.352v-2.533
                            c0-0.264-0.211-0.477-0.475-0.477c-0.263,0-0.475,0.213-0.475,0.477v2.533h-3.59c-0.66,0-1.159-0.512-1.159-1.172v-6.031
                            c0-4.088,2.702-7.646,6.624-8.73l5.872,4.025c0.06,0.615,0.572,1.119,1.229,1.137L18.753,43.896z M22.149,28.463l0.446-1.145
                            c0.121,0.006,0.542,0.008,0.631,0.004l0.445,1.141H22.149z M38.153,42.725c0,0.66-0.543,1.172-1.203,1.172h-3.605v-2.533
                            c0-0.264-0.211-0.477-0.475-0.477s-0.476,0.213-0.476,0.477v2.533h-5.343l-2.674-10.771c0.652-0.017,1.162-0.515,1.229-1.123
                            l5.9-4.039c3.922,1.083,6.645,4.642,6.645,8.729v6.031H38.153z">
                        </path>
                    </g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                </svg>
            Tư vấn
            </a>
        </li>
        <li class="group_id_menu_2 level_0"><a href="https://hailinh.vn/video.html">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g>
                    <g>
                        <path d="M152.279,426.232l-3.753-0.938l3.753-0.938c16.299-4.074,27.682-18.654,27.682-35.454
                            c0-20.152-16.394-36.547-36.546-36.547H87.631c-8.157,0-14.769,6.613-14.769,14.769s6.613,14.769,14.769,14.769h55.784
                            c3.865,0,7.008,3.144,7.008,7.009c0,3.222-2.183,6.017-5.308,6.799l-37.037,9.259c-9.349,2.337-15.878,10.7-15.878,20.335
                            c0,9.635,6.529,17.999,15.878,20.335l37.037,9.259c3.125,0.781,5.308,3.577,5.308,6.799c0,3.865-3.143,7.009-7.008,7.009H87.631
                            c-8.157,0-14.769,6.613-14.769,14.769s6.613,14.769,14.769,14.769h55.784c20.152,0,36.546-16.395,36.546-36.547
                            C179.961,444.888,168.577,430.307,152.279,426.232z">
                        </path>
                    </g>
                </g>
                <g>
                    <g>
                        <path d="M265.695,410.525h-33.706v-14.316c0-7.895,6.423-14.316,14.316-14.316h48.476c8.157,0,14.769-6.613,14.769-14.769
                            s-6.613-14.769-14.769-14.769h-48.476c-24.182,0-43.855,19.673-43.855,43.855v58.171c0,24.182,19.673,43.855,43.855,43.855h19.39
                            c24.182,0,43.855-19.673,43.855-43.855C309.549,430.198,289.877,410.525,265.695,410.525z M265.695,468.697h-19.39
                            c-7.895,0-14.316-6.422-14.316-14.316v-14.316h33.706c7.895,0,14.316,6.422,14.316,14.316S273.59,468.697,265.695,468.697z">
                        </path>
                    </g>
                </g>
                <g>
                    <g>
                        <path d="M395.284,352.354h-19.39c-24.182,0-43.855,19.673-43.855,43.855v58.171c0,24.182,19.673,43.855,43.855,43.855h19.39
                            c24.182,0,43.855-19.673,43.855-43.855v-58.171C439.138,372.027,419.466,352.354,395.284,352.354z M409.6,454.38
                            c0,7.895-6.422,14.316-14.316,14.316h-19.39c-7.895,0-14.316-6.422-14.316-14.316v-58.171c0-7.895,6.422-14.316,14.316-14.316
                            h19.39c7.895,0,14.316,6.422,14.316,14.316V454.38z">
                        </path>
                    </g>
                </g>
                <g>
                    <g>
                        <polygon points="192,122.501 192,215.183 297.922,168.842   "></polygon>
                    </g>
                </g>
                <g>
                    <g>
                        <path d="M438.154,13.765H73.846C33.127,13.765,0,46.892,0,87.611v162.462c0,40.719,33.127,73.846,73.846,73.846h364.308
                            c40.719,0,73.846-33.127,73.846-73.846V87.611C512,46.892,478.873,13.765,438.154,13.765z M340.69,182.372l-157.538,68.923
                            c-1.895,0.829-3.912,1.239-5.919,1.239c-2.831,0-5.644-0.813-8.085-2.408c-4.172-2.728-6.686-7.377-6.686-12.361V99.919
                            c0-4.984,2.514-9.632,6.686-12.361c4.17-2.728,9.438-3.169,14.004-1.17l157.538,68.923c5.375,2.352,8.849,7.663,8.849,13.531
                            S346.065,180.02,340.69,182.372z">
                        </path>
                    </g>
                </g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
            </svg>
            Video 360 độ</a></li>
        <li class="group_id_menu_2 level_0">
            <a href="https://hailinh.vn/tin-tuc.html">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.867 477.867" style="enable-background:new 0 0 477.867 477.867;" xml:space="preserve">
                    <g>
                        <g>
                            <path d="M460.8,119.467h-85.333v-102.4C375.467,7.641,367.826,0,358.4,0H17.067C7.641,0,0,7.641,0,17.067V409.6
                                c0,37.703,30.564,68.267,68.267,68.267H409.6c37.703,0,68.267-30.564,68.267-68.267V136.533
                                C477.867,127.108,470.226,119.467,460.8,119.467z M136.533,85.333h102.4c9.426,0,17.067,7.641,17.067,17.067
                                s-7.641,17.067-17.067,17.067h-102.4c-9.426,0-17.067-7.641-17.067-17.067S127.108,85.333,136.533,85.333z M290.133,409.6h-204.8
                                c-9.426,0-17.067-7.641-17.067-17.067s7.641-17.067,17.067-17.067h204.8c9.426,0,17.067,7.641,17.067,17.067
                                S299.559,409.6,290.133,409.6z M290.133,341.333h-204.8c-9.426,0-17.067-7.641-17.067-17.067c0-9.426,7.641-17.067,17.067-17.067
                                h204.8c9.426,0,17.067,7.641,17.067,17.067C307.2,333.692,299.559,341.333,290.133,341.333z M290.133,273.067h-204.8
                                c-9.426,0-17.067-7.641-17.067-17.067c0-9.426,7.641-17.067,17.067-17.067h204.8c9.426,0,17.067,7.641,17.067,17.067
                                C307.2,265.426,299.559,273.067,290.133,273.067z M290.133,204.8h-204.8c-9.426,0-17.067-7.641-17.067-17.067
                                c0-9.426,7.641-17.067,17.067-17.067h204.8c9.426,0,17.067,7.641,17.067,17.067C307.2,197.159,299.559,204.8,290.133,204.8z
                                M443.733,409.6c0,18.851-15.282,34.133-34.133,34.133s-34.133-15.282-34.133-34.133v-256h68.267V409.6z">
                            </path>
                        </g>
                    </g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                </svg>
                Tin tức
            </a>
            <span class="next_menu" id="next_1012"></span>
            <ul class="highlight highlight_1 scroll_bar" id="sub_menu_1012">
                <div class="label" id="close_1012">Tin tức</div>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/tin-hai-linh-kn.html">Tin nội bộ</a> </li>
                <li class="group_id_menu_2 level_1"><a href="https://hailinh.vn/du-an-kn.html">Dự án</a></li>
            </ul>
        </li>
    </ul>
</div>
</body>
</html>
<script src="js/main.js"></script>
